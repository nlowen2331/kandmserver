const bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 'adminAccountPass123';

bcrypt.hash(myPlaintextPassword, saltRounds).then((hash)=>{
    console.log(hash)
    bcrypt.compare('adminAccountPass123', hash).then(function(result) {
        console.log('this result should be true---> '+ result)
    });
    bcrypt.compare('adminAccountPass1234', hash).then(function(result) {
        console.log('this result should be false---> '+result)
    });
})

