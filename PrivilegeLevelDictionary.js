const define = (name, value) =>{
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("PROPETY_OWNER", 1);
define("ADMIN", 2);
