
const jwt= require('jsonwebtoken')
require('dotenv').config()

const VerifyToken = (req, res, next) => {

    console.log('Verify Token: Attempting to authorize use of protected route')

    const bearerHeader = req.headers['authorization']

    if (bearerHeader == null) {
        console.log("Verify Token: No authorization detected")
        return res.status(401).send({error: 'This is a protected route, it requires an `authorization` header'})
    }

    const bearerToken = bearerHeader.split(' ')[1]

    jwt.verify(bearerToken, process.env.SIGNING_KEY, (err, authData) => {
        if (err) {
            switch(err.name){
                case 'TokenExpiredError':
                    console.log('Verify Token: User was denied authorization due to expired credentials')
                    return res.status(403).send({error: `Your token has expired, use /Authenticate route to get new credentials`})
                default:
                    return res.status(403).send({error: 'Your token was rejected, this could be a server error'})
            }
        }

        if (authData.payload.user == null || authData.payload.privilege == null || authData.payload.id==null) {
            console.log('Verify Token: Token contained incorrect claims, this indicates a server error or a new authorization method')
            return res.status(403).send({error: `Your token contained improper/invalid/missing claims, this may indicate 
                                            the server has migrated to a new authorization method`})
        }
        console.log(`Verify Token: User's token verified`)

        req.payload = authData.payload

        next()
    })

}

module.exports=VerifyToken