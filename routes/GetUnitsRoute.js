const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const {getUnits} = require('../config')

router.get('*', (req, res, next) => {
    console.log('Server asked to run select_units')
    if (!getIsConnected() || getIsConnected()==null) {
        console.log("Connection not established... Cannot execute select_units")
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
    }
    else {
        connection().query(getUnits, (err, proc_res) => {
            if (err) {
                console.log("There was an error calling select_units")
                console.log(err)
                res.send({
                    message: "Some procedure error",
                    error_code: dictionary.PROCEDURE_ERROR
                });
            }
            else {
                console.log("Successfully sent procedure results to client!")
                res.send({ ...proc_res, message: null, error_code: dictionary.NO_ERROR })
            }
        })
    }

});


module.exports = router