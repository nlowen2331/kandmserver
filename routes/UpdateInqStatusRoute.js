const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const {update_status} = require('../config')
const VerifyToken = require('./VerifyToken')

router.post('*', VerifyToken,(req, res, next) => {

	console.log('Server asked to update status for inquiry '+req.body.inqid)
	console.log(req.body)

    if (!getIsConnected() || getIsConnected()==null) {
        console.log("Connection not established... Cannot update status for inquiry "+req.body.inqid)
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
    }
    else {
        connection().query(update_status(req.body.inqid,req.body.status,req.payload.id), (err, proc_res) => {
            console.log('proc used: ',update_status(req.body.inqid,req.body.status,req.payload.id))
            if (err) {
                console.log("There was an error updating the status for inquiry "+req.body.inqid)
                console.log(err)
                res.send({
                    message: "Some procedure error",
                    error_code: dictionary.PROCEDURE_ERROR
                });
            }
            else {
                if(proc_res.affectedRows<1) {
                    console.log('Successfully executed proc, but no rows affected')
                    res.send({
                        message: "No rows affected by update",
                        error_code: dictionary.NO_CHANGES
                    });
                }
                else{
                console.log("Successfully updated status!")
                res.send({ message: null, error_code: dictionary.NO_ERROR })
                }
            }
        })
    }

});


module.exports = router