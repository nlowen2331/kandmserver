const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const {post_comment} = require('../config')
const VerifyToken = require('./VerifyToken')
const { Validator, exp } = require('../ParamExpressions.js')

router.post('*', VerifyToken,(req, res, next) => {

	console.log('Server asked to post comment for inquiry '+req.body.inqid)
	console.log(req.body)

    if (!getIsConnected() || getIsConnected()==null) {
        console.log("Connection not established... Cannot post comment for inquiry "+req.body.inqid)
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
    }

    if(!Validator(req.body.comment,exp.noBreakCharacters) || !Validator(req.body.inqid,exp.number)) {
        console.log('Request did not contained null or improper body')
        res.send({
            message: 'Improper request',
            error_code: dictionary.IMPROPER_REQUEST
        })
        return
    }

    else {
        connection().query(post_comment(req.body.inqid,req.body.comment,req.payload.id), (err, proc_res) => {
            if (err) {
                console.log("There was an error posting a comment for inquiry "+req.body.inqid)
                console.log(err)
                res.send({
                    message: "Some procedure error",
                    error_code: dictionary.PROCEDURE_ERROR
                });
            }
            else {
                if(proc_res.affectedRows<1) {
                    console.log('Successfully executed proc, but no rows affected')
                    res.send({
                        message: "No rows affected by update",
                        error_code: dictionary.NO_CHANGES
                    });
                }
                else{
                console.log("Successfully posted comment!")
                res.send({ message: null, error_code: dictionary.NO_ERROR })
                }
            }
        })
    }

});


module.exports = router