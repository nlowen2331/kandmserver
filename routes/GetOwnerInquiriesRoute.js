const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
const VerifyToken = require('./VerifyToken')
const { connection } = require('../Connection.js')
const { getInquiriesByStatus } = require('../config')

router.get('/:status?/:count?', VerifyToken, (req, res, next) => {

    let p_status = req.params.status

    if (p_status == null || isNaN(p_status)) {
        p_status = 4
    }
    else {
        p_status = Number.parseInt(p_status)
    }

    let p_count = req.params.count
    if (p_count === 'true') {
        p_count = true
    }
    else {
        p_count = false
    }

    console.log('Server asked for inquiries of owner: '+req.payload.user+', with status less than or equal to: '+p_status+', only send count? '+p_count)

    connection().query(getInquiriesByStatus(req.payload.id, p_status), (err, proc_res) => {
        if (err) {
            console.log("There was an error selecting inquiries")
            console.log(err)
            res.send({
                message: "Some procedure error",
                error_code: dictionary.PROCEDURE_ERROR
            });
            return
        }
        
        if (!p_count) {
            console.log("Successfully retrieved owner's inquiries, sending to client")
            res.send({
                ...proc_res,
                message: null,
                error_code: dictionary.NO_ERROR
            });
        }
        else{
            console.log("Successfully retrieved owner's inquiries, count is true, sending count")
            let res_count = proc_res[0].length

            res.send({
                count: res_count,
                message: null,
                error_code: dictionary.NO_ERROR
            });
        }
    })
})



module.exports = router