const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const { update_status_msg, select_msg_statuses } = require('../config')
const VerifyToken = require('./VerifyToken')

router.post('*', VerifyToken, (req, res, next) => {

    console.log('Server asked to update status for a message' + req.body.msgid)
    console.log(req.body)

    if (!getIsConnected() || getIsConnected() == null) {
        console.log("Connection not established... Cannot update status for message " + req.body.msgid)
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
        return
    }

    connection().query(select_msg_statuses(req.body.msgid), (err, proc_res1) => {

        if (err) {
            console.log("There was an error updating the status for message " + req.body.msgid)
            console.log(err)
            res.send({
                message: "Some procedure error",
                error_code: dictionary.PROCEDURE_ERROR
            });
            return
        }

        console.log(proc_res1[0][0].statuses)
        let data = JSON.parse(proc_res1[0][0].statuses)
        data.every(e=>{
            if(e.ownerid===req.payload.id) {
                e.status=req.body.status
                console.log('Updated status of message.statuses')
                return false
            }
            return true
        })

        console.log('data: ',data)

        data=JSON.stringify(data)

        connection().query(update_status_msg(req.body.msgid, data), (err, proc_res2) => {
            console.log('proc used: ', update_status_msg(req.body.msgid, data))
            if (err) {
                console.log("There was an error updating the status for message " + req.body.msgid)
                console.log(err)
                res.send({
                    message: "Some procedure error",
                    error_code: dictionary.PROCEDURE_ERROR
                });
            }
            else {
                if (proc_res2.affectedRows < 1) {
                    console.log('Successfully executed proc, but no rows affected')
                    res.send({
                        message: "No rows affected by update",
                        error_code: dictionary.NO_CHANGES
                    });
                }
                else {
                    console.log("Successfully updated status!")
                    res.send({ message: null, error_code: dictionary.NO_ERROR })
                }
            }
        })
    })

});


module.exports = router