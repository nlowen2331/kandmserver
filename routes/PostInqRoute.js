const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const {postInquiry} = require('../config')
const {sendClientEmail} = require('../Emailer')

router.post('*', (req, res, next) => {


	console.log('Server asked to post inquiry')

    if (!getIsConnected() || getIsConnected()==null) {
        console.log("Connection not established... Cannot execute insert_inquiry")
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
    }
    else {
        connection().query(postInquiry(JSON.stringify(req.body.json),req.body.id), (err, proc_res) => {
            if (err) {
                console.log("There was an error calling post_inquiry")
                console.log(err)
                res.send({
                    message: "Some procedure error",
                    error_code: dictionary.PROCEDURE_ERROR
                });
                return
            }

            console.log("Successfully posted inquiry!")
            res.send({ ...proc_res, message: null, error_code: dictionary.NO_ERROR })

            console.log(`Attempting to email ${req.body.json.email}`)
            sendClientEmail(req.body.json.email,req.body.id,'confirmation')


        })
    }

});


module.exports = router