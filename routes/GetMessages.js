const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const { getMessages, get_messages_with_status } = require('../config')
const VerifyToken = require('./VerifyToken.js')
const { Validator } = require('../ParamExpressions.js')

router.get('/:status?', VerifyToken, (req, res, next) => {
    console.log('Server asked to run get messages')
    if (!getIsConnected() || getIsConnected() == null) {
        console.log("Connection not established... Cannot execute select_units")
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
        return
    }

    let p_status = req.params.status
    if(p_status==null||isNaN(p_status)) {
        p_status=4
    }
    else{
        p_status=Number.parseInt(p_status)
    }

    connection().query(getMessages, (err, proc_res) => {
        if (err) {
            console.log("There was an error getting messgaes from server")
            console.log(err)
            res.send({
                message: "Some procedure error",
                error_code: dictionary.PROCEDURE_ERROR
            });
        }
        else {

            const data = proc_res[0]
            const response = []
            data.forEach(e => {

                let status = JSON.parse(e.statuses)
                let f_status = -1
                status.every(e => {
                    if (e.ownerid === req.payload.id) {
                        f_status = e.status
                        return true
                    }
                    return false
                })

                if (f_status === -1 || f_status>p_status) {
                    return
                }

                let r = {
                    messageid: e.messageid,
                    firstname: e.firstname,
                    lastname: e.lastname,
                    email: e.email,
                    phone: e.phone,
                    message: e.message,
                    status: f_status,
                    dateof: e.dateof
                }

                response.push(r)

            })


            console.log("Successfully sent procedure results to client!")
            res.send({ res: response, message: null, error_code: dictionary.NO_ERROR })
        }
    })
})


module.exports = router