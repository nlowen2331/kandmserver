const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const {get_calender_by_unit} = require('../config')
const { exp, Validator } = require('../ParamExpressions.js')

router.get('/:unitid', (req, res, next) => {
    console.log('Server asked to select calender for specific unit')
    if (!getIsConnected() || getIsConnected()==null) {
        console.log("Connection not established... Cannot execute stored proc")
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
    }

    if(!Validator(req.params.unitid,exp.unitid)) {
        console.log("The request contained an improper body")
        res.send({
            message: "The request contained an improper body",
            error_code: dictionary.IMPROPER_REQUEST
        });
        return
    }

    else {
        connection().query(get_calender_by_unit(req.params.unitid), (err, proc_res) => {
            if (err) {
                console.log("There was an error calling the proc")
                console.log(err)
                res.send({
                    message: "Some procedure error",
                    error_code: dictionary.PROCEDURE_ERROR
                });
            }
            else {
                console.log("Successfully sent procedure results to client!")
                res.send({ ...proc_res, message: null, error_code: dictionary.NO_ERROR })
            }
        })
    }

});


module.exports = router