const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')

router.get('*', (req,res,next)=>{
	console.log('Server was pinged')
	res.send({
		message: "Pong!",
		error: dictionary.NO_ERROR
	});
});


module.exports = router