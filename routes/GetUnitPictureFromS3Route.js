const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
require('dotenv').config()
const VerifyToken = require('./VerifyToken')
const { connection } = require('../Connection.js')
const { verify_unit_owner } = require('../config.js')
const {exp, Validator} = require('../ParamExpressions.js')
const GetPictureFromS3 = require('../GetPictureFromS3')

router.get('/:unitid/:key', VerifyToken, (req, res, next) => {

    console.log(`Server asked to get pictures of units from S3: ${req.params.unitid}/${req.params.key}`)
    
    if(!Validator(req.params.unitid,exp.unitid)||!Validator(req.params.key,exp.s3Key)) {
        console.log('Request did not contained null or improper params')
        res.send({
            message: 'Improper request',
            error_code: dictionary.IMPROPER_REQUEST
        })
        return
    }

    //Check if unit exists and owner is proper

    connection().query(verify_unit_owner(req.params.unitid,req.payload.id),(err,proc_res)=>{
        if(err) {
            console.log('There was a DB error verifying unit: '+req.params.unitid)
            console.log(err)
            res.send({
                message: 'DB Error',
                error_code: dictionary.PROCEDURE_ERROR
            })
            return
        }

        if(proc_res[0].length<1) {
            console.log('No matching unit/owner for unit: '+req.params.unitid)
            res.send({
                message: 'No matching unit/owner',
                error_code: dictionary.NO_MATCHING_DATA
            })
            return
        }

        const unit = proc_res[0][0].unitid
        const key = req.params.key

        GetPictureFromS3(`${unit}/${key}`).then(proc_res=>{
            console.log(`Successfully obtained image for ${unit}/${key}`)
            res.set({
                'Content-Length': proc_res.ContentLength,
                'Content-Type': proc_res.ContentType,
                'Accept-Ranges' : proc_res.AcceptRanges,
                'Last-Modified' : proc_res.LastModified
            })
            res.send(proc_res.Body)
        }).catch(err=>{
            console.log(`Error obtaining image for ${unit}/${key}`)
            console.log(err)
            res.send({
                message: 'Success',
                error_code: dictionary.UNKNOWN_ERROR
            })
        })


    })
})



module.exports = router