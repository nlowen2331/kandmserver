const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
const fetchCalender = require('../bnbhostapi/getCalendar');
const { exp, Validator } = require('../ParamExpressions.js');

router.get('/:airbnbid', (req, res, next) => {
    const airbnbid = req.params.airbnbid
    console.log("Server asked to get disabled days from Airbnb")

    if(!Validator(airbnbid,exp.airbnbid)) {
        console.log("Improper airbnb id")
        res.send({
            message: "Improper airbnb id",
            error_code: dictionary.IMPROPER_REQUEST
        });
        
        return
    }

    fetchCalender(airbnbid).then((data)=>{
        
        console.log("Successfully retrieved calender, sending response to client")
        res.send({
            message: null,
            error_code: dictionary.NO_ERROR,
            calender: data
        });


    }).catch(err=>{
        console.log("Could not fetch calender from Airbnb...")
        console.log(err)
        res.send({
            message: 'Could not fetch data from end point',
            error_code: dictionary.AIRBNB_API
        });
    })

});


module.exports = router