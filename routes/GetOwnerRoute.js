const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
const VerifyToken = require('./VerifyToken')
const { connection} = require('../Connection.js')
const { select_owner } = require('../config')

router.get('*', VerifyToken, (req, res, next) => {

    console.log(req.payload.user)

    connection().query(select_owner(req.payload.id), (err, proc_res) => {
        if (err) {
            console.log("There was an error selecting owner")
            console.log(err)
            res.send({
                message: "Some procedure error",
                error_code: dictionary.PROCEDURE_ERROR
            });
            return
        }
        console.log("Successfully retrieved owner, sending to client")
        res.send({
            ...proc_res,
            message: null,
            error_code: dictionary.NO_ERROR
        });
    })
})



module.exports = router