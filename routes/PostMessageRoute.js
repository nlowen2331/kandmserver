const express = require('express')
const router = express.Router()
const { connection, getIsConnected } = require('../Connection.js')
const dictionary = require('../ErrorCodeDictionary.js')
const { postMessage } = require('../config')
const {getIdsLoaded,getOwnerIds} = require('../GetOwnerIds')

router.post('*', (req, res, next) => {

    console.log('Server asked to post message')
    console.log(req.body)

    if (!getIsConnected() || getIsConnected() == null) {
        console.log("Connection not established... Cannot execute insert_message")
        res.send({
            message: "Could not establish connection",
            error_code: dictionary.NO_CONNECTION
        });
        return
    }

    if (!getIdsLoaded()) {
        console.log(`Server could not handle a message post request because the server has not recieved all ownerid's from the database.\nThis error should only occur within seconds of the server being started`)
        res.send({
            message: "The server has not been properly established",
            error_code: dictionary.YOUNG_SERVER
        });
        return
    }

    let json=getOwnerIds().map(e=>({ownerid: e.ownerid,status: 0}))
    json = JSON.stringify(json)
    console.log('Appending UNREAD statuses for each owner onto this message...')
    console.log(json)

    connection().query(postMessage(req.body.fname, req.body.lname, req.body.email, req.body.phone, req.body.message,json), (err, proc_res) => {
        if (err) {
            console.log("There was an error calling post_message")
            console.log(err)
            res.send({
                message: "Some procedure error",
                error_code: dictionary.PROCEDURE_ERROR
            });
        }
        else {
            console.log("Successfully posted message!")
            res.send({ ...proc_res, message: null, error_code: dictionary.NO_ERROR })
        }
    })


});


module.exports = router