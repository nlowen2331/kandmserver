const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
require('dotenv').config()
const VerifyToken = require('./VerifyToken')

router.get('*', VerifyToken, (req, res, next) => {

    res.send({
        user: req.payload.user,
        message: "Token accepted, login is valid",
        error_code: dictionary.NO_ERROR
    })
})



module.exports = router