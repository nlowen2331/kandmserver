
const { GetBucketObject } = require('../fetchers/S3Fetcher')
const {SculptClientEmail,SendEmail} = require('../senders/Emailer')


/*
    Takes in an inquiry (including unitid)
    Sculpts a confirmation email,
    then sends that email to the returned recipient

*/
const SendAcceptanceEmail = async (params) =>{

    let EmailResponse=await SculptClientEmail({...params},'acceptance')
    
    //attach lease to email
    if(params.shouldSendLease){

        console.log('Routine AcceptReservationRoutine: Will attach lease to email ')

        let Lease= (await GetBucketObject(`${params.unitid}.pdf`,process.env.LEASE_BUCKET||`kandmleases`)).Body

        EmailResponse.attachments=[
            {
                filename: `${params.unitid}_Lease.pdf`,
                content: Lease
            }
        ]

    }

    await SendEmail({
        ...EmailResponse,
        subject: 'We have Accepted your Reservation!'
    })

    return({OK: true})
    
}

module.exports=SendAcceptanceEmail