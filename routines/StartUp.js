const { CreateS3Agent } = require("../fetchers/S3Fetcher")
const CreateExpressServer = require("../listeners/RequestListener")
const { CreatePool } = require("../writers/DBWorker")
const {CacheOwnerIds} = require("./CacheOwnerIds")
const {CacheUnitIds} = require("./CacheUnitIds")
const {CreateEmailer} = require('../senders/Emailer')

const StartUp = async () =>{
    console.log('Running Routine: StartUp')
    let promises = await Promise.all([CreateExpressServer(), CreatePool(),CreateS3Agent(),CacheOwnerIds(),CacheUnitIds(), CreateEmailer()])
    promises.forEach(promise=>{
        console.log(promise)
    })
    return('Routine StartUp: Startup successful')  
}

module.exports=StartUp