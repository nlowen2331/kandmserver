
const {SculptClientEmail,SendEmail} = require('../senders/Emailer')


/*
    Takes in an inquiry (including unitid)
    Sculpts a confirmation email,
    then sends that email to the returned recipient

*/
const ConfirmInquiry = async (params) =>{

    let Response
    let error='none'
    try{
        Response = await SculptClientEmail(params,'confirmation')
    }
    catch(err) {
        error=err
    }
    if(error!=='none') {
        console.log('Routine ConfirmInquiry: There was an error sculpting the confirmation email from template')
        return({OK: false,error: error})
    }

    let SendResponse
    try{
        SendResponse = await SendEmail({
            ...Response,
            subject: 'You Requested a Reservation!'
        })
    }
    catch(err) {
        error=err
    }
    if(error!='none') {
        console.log('Routine ConfirmInquiry: There was an error sending an email to the client')
        return({OK: false,error: error})
    }

    return({OK: true})
    
}

module.exports=ConfirmInquiry