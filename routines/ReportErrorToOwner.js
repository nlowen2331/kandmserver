


const { StoredProcedures } = require("../writers/DBStoredProcs")
//Reports error to owner as a new message in the dashboard

const { UseProcedure } = require("../writers/DBWorker")
const { _GetOwnerIdArray } = require("./CacheOwnerIds")



const ReportErrorToOwner = async (params,message) =>{

    console.log('Routine ReportErrorToOwner: Sending error message to owner')

    //do the stupid thing that is setting the message to
    //unread for each owner id
    let statuses = JSON.stringify(_GetOwnerIdArray().map(owner_json=>({status: 0, ...owner_json})))

    let FullMessage = `This message was generated on behalf of the client 
                        by the server in response to an error that occured 
                        while processing their information: ${message} (Inquiry ID: ${params.id})`
    
    //create the db info

    let DBRes
    let error='none'

    DBRes = await UseProcedure(StoredProcedures.insert_msg,[params.fname,params.lname,params.email,
                            params.phone,FullMessage,statuses])

    console.log('Routine ReportErrorToOwner: Successfully posted error message')


}

module.exports=ReportErrorToOwner