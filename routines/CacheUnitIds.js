const { StoredProcedures } = require("../writers/DBStoredProcs")
const { UseProcedure } = require("../writers/DBWorker")

global.UnitIdArray=null
global.UnitIdArrayIsLoaded = false

const CacheUnitIds = async () =>{
    let _UnitArray = await UseProcedure(StoredProcedures.select_units,null,true)
    if(_UnitArray.length<1) {
        throw new Error('No unitids retrieved from the DB')
    }

    let _UnitIdArray = []

    _UnitArray.forEach(row=>{
        _UnitIdArray.push(row.unitid)
    })

    global.UnitIdArray=_UnitIdArray
    global.UnitIdArrayIsLoaded=true
    return (`Successfully globally assigned 'UnitIdArray'`)
}

module.exports= {CacheUnitIds}
