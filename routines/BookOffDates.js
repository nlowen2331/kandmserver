
const PostCalenderV2 = require("../bnbhostapi/postCalender")
const { StoredProcedures } = require("../writers/DBStoredProcs")
const { UseProcedure } = require("../writers/DBWorker")

//accept arrival + departure date
const BookOffDates = async ({arrival,departure,unitid}) =>{

    //find airbnb id
    let BnBID = (await UseProcedure(StoredProcedures.select_single_unit_bnbid,unitid,true))[0].airbnbid

    await PostCalenderV2({arrival: arrival,departure: departure,listing_id: BnBID})

    return({OK:true})

}

module.exports=BookOffDates