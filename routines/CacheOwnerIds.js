const { StoredProcedures } = require("../writers/DBStoredProcs")
const { UseProcedure } = require("../writers/DBWorker")

//****This file features legacy support, that is why it's redundant */

let OwnerIdArray
let OwnerIdArrayIsLoaded = false
global.OwnerIdArray=OwnerIdArray
global.OwnerIdArrayIsLoaded=false

//Legacy support
const _GetOwnerIdArray = () =>{return OwnerIdArray}
const _GetOwnerIdArrayIsLoaded = () =>{return OwnerIdArrayIsLoaded}

const CacheOwnerIds = async () =>{
    OwnerIdArray = await UseProcedure(StoredProcedures.select_all_ownerids,null,true)
    if(OwnerIdArray.length<1) {
        throw new Error('No owners retrieved from the DB')
    }
    global.OwnerIdArray=OwnerIdArray
    OwnerIdArrayIsLoaded=true
    global.OwnerIdArrayIsLoaded=true
    return (`Successfully globally assigned 'OwnerIdArray'`)
}

module.exports= {CacheOwnerIds,_GetOwnerIdArray,_GetOwnerIdArrayIsLoaded}
