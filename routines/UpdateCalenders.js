
const dayjs = require('dayjs')
const fetchCalender = require('../bnbhostapi/getCalendar')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')
const currency = require('currency.js')
const LogProcedure = require('../loggers/ErrorReporter')

//This routine updates all the calenders in the DB to match Airbnb
const UpdateCalenders = async () => {

    console.log('Running Routine: UpdateCalenders')
    //Retrieve ids from DB
    let bnbIDs = await UseProcedure(StoredProcedures.select_unit_airbnbid, null, true)

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Retrieved AirBnB ids')
    }

    if (bnbIDs.length < 1) {
        return ('Routine UpdateCalenders: There were no available units in the DB with Airbnb IDs')
    }

    if (DEBUG_ALL) {
        console.log('Logging bnbIDs')
        console.log(bnbIDs)
    }

    let WillUpdate = [] //All rows that will update their calenders

    //Filter out rows that don't want update
    bnbIDs.forEach(row => {
        if (row.airbnbid == null || row.noSync === 1) {
            return
        }
        WillUpdate.push(row)
    })

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Decided what units will update')
    }

    if (DEBUG_ALL) {
        console.log('Logging all rows-to-update')
        console.log(WillUpdate)
    }

    //Get each row's updated calender
    let updatedCalenders = await Promise.all(WillUpdate.map(async (row) => {
        let calender = await fetchCalender(row.airbnbid)
        return { unitid: row.unitid, days: calender }
    }))


    //Get each row's price manipulators

    let ManipulatorResArray = await Promise.all(WillUpdate.map(async (row)=>{
        let DB_Res = await UseProcedure(StoredProcedures.select_pricem_by_unitid,row.unitid,true)
        DB_Res=DB_Res[0]

        return {unitid: DB_Res.unitid,priceManipulators: DB_Res!=null ? JSON.parse(DB_Res.priceManipulators) : []}
    }))

    if(DEBUG_PRICEM){
        console.log(`Logging Price Manipulators`)
        console.log(ManipulatorResArray)
    }


    if(DEBUG_TIME_HAWK) {
        console.log(`TIME HAWK: Retrieved each row's calender`)
    }


    if (DEBUG_ALL) {
        console.log('Logging all fetched calenders')
        console.log(updatedCalenders)
    }


    //Map bnb response to a new format for our app
    let formattedCalenders = []
    try {
        formattedCalenders = await Promise.all(updatedCalenders.map(async (calender) => {
            let formatted_calender = await FormatCalender(calender.days)
            return { unitid: calender.unitid, days: formatted_calender }
        }))
    }
    catch (err) {
        throw err
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Formatted calenders for use in our app')
    }

    if (DEBUG_ALL) {
        console.log('Logging formatted calenders')
        console.log(formattedCalenders)
    }

    //Merge Calender with DB counterpart

    let mergedCalenders = []
    try {
        mergedCalenders = await Promise.all(formattedCalenders.map(async (calender) => {
            let merged_calender = await MergeDBCalender(calender.unitid, calender.days)
            return { unitid: calender.unitid, days: merged_calender }
        }))
    }
    catch (err) {
        throw err
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Merged formatted calender from AirBnB with our calenders')
    }

    let error = false

    //Update min/max prices
    let priceSuccessArray = []
    try {
        priceSuccessArray = await Promise.all(mergedCalenders.map(async (calender) => {
            let correct_unit_res=ManipulatorResArray.find(man_res=>man_res.unitid===calender.unitid)
            return await UpdateMinMaxPrices(calender.unitid, calender.days,correct_unit_res.priceManipulators)
        }))
    }
    catch (err) {
        throw err
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Updated min/max prices')
    }

    if (DEBUG_ALL) {
        console.log('Logging calenders that will be inserted')
        console.log(mergedCalenders)
    }

    //Insert merged calenders
    let insertSuccessArray = []

    try {
        insertSuccessArray = await Promise.all(mergedCalenders.map(async (calender) => {
            let success = await InsertFormattedCalender(calender.unitid, calender.days)
            return success
        }))
    }

    catch (err) {
        throw err
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Inserted calenders')
    }

    if (error) {
        return (`Routine UpdateCalenders: Routine unsuccessful, could not successfully update calenders `)
    }

    return (`Routine UpdateCalenders: Successfully updated all calenders`)


}

//Put {unitid,airbnb_res.days} into our DB format
//{(unitid):(appended_unitid), days: [{date: somedate},{date: somedate2}]} 
//willLog===true ---> The routine will log its actions
const FormatCalender = async (bnb_res) => {
    let formattedDays = bnb_res.days.map((day) => ({ date: day.date, available: day.available, price: day.price.local_price, syncPrice: true, priceMultipliers: [] }))
    return formattedDays
}


//Takes in a formatted calender and asserts the minimum and maximum prices in the array of days
const FindMinMaxPrices = async (formatted_calender,price_manipulators) => {

    //capture any errors for reporting
    let ReportedErrors=[]

    //clone array
    let check_calender=formatted_calender.map(calender_entry=>(
        {...calender_entry}
    ))

    //Apply price manipulation to check_calender (cloned)
    if(price_manipulators!=null&&price_manipulators.length>1) {
        //iterate through each manipulator in the db
        price_manipulators.forEach(price_manipulator=>{
            //iterate through each date in the manipulator
            price_manipulator.dates.forEach(man_date=>{
                //find the manipulator date in the check_calender
                let calender_entry=check_calender.find(calender_entry=>dayjs(calender_entry.date).isSame(dayjs(man_date),'day'))
                //apply price manipulation to the check_calender entry
                if(calender_entry!=null){

                    if(typeof price_manipulator.value==='number'&&price_manipulator.value===price_manipulator.value) {
                        //find operation to apply
                        switch(price_manipulator.operation){
                            case '+':
                                calender_entry.price= (currency(calender_entry.price).add(price_manipulator.value)).value
                                break;
                            case '-':
                                calender_entry.price= (currency(calender_entry.price).subtract(price_manipulator.value)).value
                                break;
                            case '*':
                                calender_entry.price= (currency(calender_entry.price).multiply(price_manipulator.value)).value
                                break;
                            case '/':
                                calender_entry.price= (currency(calender_entry.price).divide(price_manipulator.value)).value
                                break;
                            default:
                                ReportedErrors.push({msg: `Price manipulator contained invalid operation: ${price_manipulator.operation}`})
                                break;
                        }

                        if(DEBUG_PRICEM){
                            console.log(`Price manipulation affected date: ${calender_entry.date}`)
                            console.log(`New Price: $${currency(calender_entry.price).toString()}`)
                        }
                    }
                    //The value is invalid
                    else{
                        ReportedErrors.push({msg: `Price manipulator contained invalid value: ${price_manipulator.value}`})
                    }
                    
                }
                //If the date is not found, log the error
                else{
                    ReportedErrors.push({msg: `Price manipulator contained a date that could not be found in the calender: ${man_date}`})

                    //Known occurences: When a date is passed (today>date) this error will be thrown, because airbnb reports from today onward
                    //Solutions: Throw out dates that expire
                }
            })
        
        })

    }

    let minprice = 1000000
    let maxprice = 0
    check_calender.forEach((day) => {

        if (day.price > maxprice)
            maxprice = day.price
        if (day.price < minprice)
            minprice = day.price
    })

    //If prices could not be determined return a bad json
    if (minprice === 100000 || maxprice === 0) {
        return ({ OK: false,errors: ReportedErrors })
    }

    //return normally
    return ({ OK: true, minPrice: minprice, maxPrice: maxprice,errors: ReportedErrors })

}

//Post the minimum and maximum prices for a particular unit in the DB

const UpdateMinMaxPrices = async (unitid, formatted_calender,price_manipulators) => {
    //Obtain the prices
    let json = await FindMinMaxPrices(formatted_calender,price_manipulators)

    if(DEBUG_MIN_MAX_PRICES) {
        console.log(`Min/Max Prices for unit ${unitid}`)
        console.log(json)
    }

    if(DEBUG_PRICEM){
        console.log(`Price manipulators for unit: ${unitid}`)
        console.log(price_manipulators)
    }

    //If there were errors send log procedure
    if(json.errors.length>1){
        console.log('Routine UpdateCalenders: Errors when updating min/max prices, check MinMaxPriceError in reports')
        try{
            LogProcedure({unit: unitid,errors: json.errors},'MinMaxPriceError')
        }
        catch(err){
            console.log('There was an error creating the Min/Max Price Error Report')
            console.log(err)
        }
        
    }

    //throw error if no min/max prices could be dertermined
    if (json.OK) {
        let response = await UseProcedure(StoredProcedures.insert_min_max_prices, [unitid, json.minPrice, json.maxPrice])
        if (response.affectedRows === 0) {
            return (`Updated unit ${unitid} min/max prices Successfully, but no rows were changed`)
        }
        return (`Updated unit ${unitid} min/max prices Successfully`)
    }
    else{
        throw new Error('Could not find Min/Max Prices for unit: '+unitid)
    }

}

//Merge a formatted calender with an existing DB calender
//DB calender price takes priority if syncPrice is false
const MergeDBCalender = async (unitid, formatted_calender) => {
    //Get any calender data for specified unitid from DB
    let DB_calender = await UseProcedure(StoredProcedures.select_calender_by_unitid, unitid, true)
    //Put it in JSON format
    DB_calender = JSON.parse(DB_calender[0].calender).days
    //If there is no existing calender, then formatted calender should be passed in
    if(DB_calender==null) {
        return formatted_calender
    }

    if (DEBUG_ALL) {
        console.log('Logging DB calender')
        console.log(DB_calender)
    }

    let mergedCalender = []

    //Parse through calender, merge the DB calender with the bnb calender
    //prioritize db_info 
    DB_calender.forEach(db_day => {
        let db_date = dayjs(db_day.date)
        let willSync = db_day.syncPrice
        let db_multiplier = db_day.priceMultipliers||JSON.stringify([])

        formatted_calender.forEach(bnb_day => {
            let bnb_date = dayjs(bnb_day.date)

            if (db_date.isSame(bnb_date)) {
                //If syncPrice is false, then do not override price
                if (!willSync) {
                    bnb_day.syncPrice = false
                    bnb_day.price = db_day.price
                }
                //Set multiplier to db value
                bnb_day.priceMultipliers=db_multiplier

                mergedCalender.push(bnb_day)
            }

        })
    })

    return mergedCalender


}

//Insert a formatted calender into the DB

const InsertFormattedCalender = async (unitid, formatted_calender) => {

    let res = UseProcedure(StoredProcedures.insert_calenders, [unitid, JSON.stringify({ days: formatted_calender})])
    if (res.affectedRows < 1) {
        return (`Updated unit ${unitid} calenders Successfully, but no rows were changed`)
    }
    return (`Updated unit ${unitid} calenders Successfully`)
}

module.exports = UpdateCalenders