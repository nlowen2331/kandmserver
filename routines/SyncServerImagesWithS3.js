const { SyncS3Contents, SyncDBWithServerImages, RemoveOutdatedFiles } = require('../writers/ImageSynchronizer')

const SyncServerImagesWithS3 = async () => {
    console.log('Running Routine: SyncServerImagesWithS3')
    let OK = false
    let _supplied =[]
    let loops = 0
    let error = 'none'
    let sync_res

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Syncing server images, begin loop')
    }

    while(loops<2&&!OK) {
        sync_res = await SyncS3Contents(_supplied,process.env.PICTURE_BUCKET).catch(err => {
            error=err
        })
        if(error!=='none') {
            break
        }
        OK=sync_res.OK
        _supplied=sync_res.data

        if(DEBUG_TIME_HAWK) {
            console.log(`TIME HAWK: Syncing server images, iteration ${loops} `)
        }

        loops++
    }
    //Threw error
    if(error!=='none') {
        return(error)
    }
    //Success
    else if(OK) {
        console.log("Routine SyncServerImagesWithS3: Synced S3 Images with server images, attempting to update DB")
        let db_res

        try{
            db_res = await SyncDBWithServerImages(sync_res.data)
        }
        catch(err){
            error=err
        }
        if(error!=='none') {
            return(error)
        }

        if(DEBUG_TIME_HAWK) {
            console.log('TIME HAWK: Sycned S3 with DB successfully')
        }

        //Delete all files that don't agree with S3 Listing
        let DeleteRes = await RemoveOutdatedFiles()
        console.log(`Routine SyncServerImagesWithS3: ${DeleteRes}`)

        //DB updated successfully
        return ("Routine SyncServerImagesWithS3: " +db_res)
        
    }
    //Could not resolve
    else{
        return('Routine SyncServerImagesWithS3: Routine unsuccessful, could not sync from S3')
    }
}

module.exports = SyncServerImagesWithS3