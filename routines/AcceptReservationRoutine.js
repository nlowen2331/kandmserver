

const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')
const BookOffDates = require('./BookOffDates')
const ReportErrorToOwner = require('./ReportErrorToOwner')
const SendAcceptanceEmail = require('./SendAcceptanceEmail')


/*
    Takes in an inquiry (including unitid)
    Sculpts a confirmation email,
    then sends that email to the returned recipient

*/
const AcceptReservationRoutine = async (params) =>{

    console.log('Routine AcceptReservation: Performing tasks as a result of a Inquiry status being changed to ACCEPTED')

    let Unit

    try{
        Unit= (await UseProcedure(StoredProcedures.select_inq_by_id,params.id,true))[0].unitid
    }
    catch(err){
        ReportErrorToOwner(params,
            `ERROR: Could not determine the corresponing Unit for this client's inquiry. This requires you to acknowledge that the client's reservation 
            was accepted. This client would also not have their dates automatically blocked on 
            the AirBnB calender and would not have been sent a lease/acceptance email`).catch(err=>{
                console.log('ReportErrorToOwner failed')
                console.log(err)
            })

        throw new Error(err)
    }

    //append params with unitid
    params.unitid=Unit

    let AcceptanceEmailPromise =null
    let BookingPromise=null

    if(params.shouldSendAcceptance){

        console.log('Routine AcceptReservation: Attempting to send acceptance email to client')
        if(params.shouldSendLease){
            console.log('Routine AcceptReservation: Will attach lease to email')
        }

        //Send acceptance acknowledgement
        AcceptanceEmailPromise= SendAcceptanceEmail(params).then(res=>{
            console.log('Routine AcceptReservation: Successfully sent acceptance email')
        }).catch(err=>{
            console.log('Routine AcceptReservation: There was an error sending acceptance email, will report error to owner')
            console.log(err)
            ReportErrorToOwner(params,`ERROR: When attempting to send acceptance acknowledgement to the client, there was an error.
                                        This client may not be aware their reservation request was accepted and will not
                                        have access to a signable lease. This error could be caused by a Unit existing
                                        with no lease. You may want to make sure a lease exists for this unit`).catch(err=>{
                console.log(`Routine AcceptReservation: An error occured on attempt to post an error to the DB `)
                console.log(err)
            })
        })
        
    }
    if (params.shouldBookDates){

        console.log('Routine AcceptReservation: Attempting to book dates on AirBnB calender')

        //Change availability on airbnb calender
        BookingPromise= BookOffDates(params).then(res=>{
            console.log('Routine AcceptReservation: Succesfully booked off dates on AirBnB')
        }).catch(err=>{
            console.log('Routine AcceptReservation: There was an error booking dates on AirBnB, will report error to owner')
            console.log(err)
            ReportErrorToOwner(params,`ERROR: When attempting to update availability for the dates booked by the client, an error occured.
                                        This client''s reserved dates may not be reflected on our calender or calenders 
                                        on other websites`).catch(err=>{
                console.log(`Routine AcceptReservation: An error occured on attempt to post an error to the DB `)
                console.log(err)
            })
        })
    }

    await Promise.all([AcceptanceEmailPromise,BookingPromise])
    

    return({OK: true})


}

module.exports=AcceptReservationRoutine