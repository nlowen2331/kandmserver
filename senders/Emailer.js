const nodemailer = require("nodemailer")
const cheerio = require('cheerio')
const { UseProcedure } = require("../writers/DBWorker")
const { StoredProcedures } = require("../writers/DBStoredProcs")
const currency = require('currency.js')
const { GetBucketObject } = require('../fetchers/S3Fetcher')

let Transporter
let WorkerIsRunning=false

const CreateEmailer = async ()=>{
    Transporter=nodemailer.createTransport({
        host: "email-smtp.us-east-2.amazonaws.com",
        port: 587,
        secure: false,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS, 
        }
    })
    WorkerIsRunning=true

    return('Successfully created emailer')
}

/*
    Create a confirmation email by filling in confirmation template from server

    This function should try-catched

    inquiry is a json object with {
        unitid,arrival,departure,taxedPrice,guests,pets,phone
    }

*/
const SculptClientEmail = async (inquiry,type)=>{

    if(DEBUG_EMAILER) {
        console.log('Server attemtping to sculpt client email')
        console.log('Logging inquiry object')
        console.log(inquiry)
    }

    let EmailTemplate = null

    switch(type){
        case 'acceptance':
            EmailTemplate='AcceptanceTemplate.html'
            break;
        case 'confirmation':
            EmailTemplate='ConfirmationTemplate.html'
            break;
        default:
            throw new Error(`Unknown email type: ${type} in SculptClientEmail`)
    }

    //Get unit info from inquiry id
    let DBRes= await UseProcedure(StoredProcedures.select_unit_by_id,inquiry.unitid,true)
    if(DBRes.length<1) {throw new Error(`No unit data was found for inquiry`)}
    let Unit = DBRes[0]
    let UnitSpecs = JSON.parse(Unit.specifications)
    //Get owner email from unit info
    let DBResOwner= await UseProcedure(StoredProcedures.select_owner,Unit.ownerid,true)
    if(DBRes.length<1) {throw new Error(`No owner data was found for inquiry unit`)}
    let OwnerPhone = DBResOwner[0].phone
    let OwnerEmail = DBResOwner[0].email
    //Get the EmailTemplate from the S3 Bucket
    let Template = await GetBucketObject(EmailTemplate,process.env.TEMPLATE_BUCKET||'kandmemails')

    if(DEBUG_EMAILER) {
        console.log('Logging Bucket Object')
        console.log(Template)
    }

    let HTMLBody = Template.Body

    if(DEBUG_EMAILER) {
        console.log('Logging Bucket Object Body')
        console.log(Template)
    }

    //Parse it with the HTML Parser
    const $ = cheerio.load(HTMLBody)

    //Replace all template text variables with correct messages

    //unitname
    $('#unit_name').text(Unit.unitname)
    //dates
    let arrival = new Date(inquiry.arrival)
    let departure = new Date(inquiry.departure)
    arrival.getDay()
    $('#dates').text(`${DayOfWeekConverter(arrival.getDay())} ${arrival.getMonth()+1}/${arrival.getDate()}/${arrival.getFullYear()}
        to ${DayOfWeekConverter(departure.getDay())} ${departure.getMonth()+1}/${departure.getDate()}/${departure.getFullYear()}`)
    //location
    $('#location').text(`${UnitSpecs.city}, ${UnitSpecs.state}`)
    //cost
    $('#cost').text(`$${currency(inquiry.totalPriceTaxed).toString()}`)
    //guests
    let guestPlurality='guests'
    if(inquiry.guests<2){guestPlurality="guest"}
    $('#guests').text(`${inquiry.guests} ${guestPlurality}`)
    //pets
    switch(inquiry.pets) {
        case 0:
            $('#pets').text(`no pets`)
            break;
        case 1:
            $('#pets').text(`1 pet`)
            break;
        default:
            $('#pets').text(`${inquiry.pets} pets`)
            break;
    }
    //clientphone
    $('#client_phone').text(`${inquiry.phone}`)
    //ownerphone
    $('#owner_phone').text(`${OwnerPhone}`)

    if(DEBUG_EMAILER) {
        console.log('Made all Jqueries with no thrown errors')
        console.log('Logging full html')
        console.log($.root().html())
        console.log('Logging full text')
        console.log($.root().text())
    }

    //Return rendered HTML

    return ({ html: $.root().html(), text: $.root().text(), to: inquiry.email,from: OwnerEmail})
}

/*
    Params is 
    -html
    -text
    -subject 
    -to
*/

const SendEmail = async (params)=>{

    //Check if worker is running
    if(!WorkerIsRunning) {
        throw new Error('Emailer has not been initialized')
    }

    //Send the email 

    let options = {
        from: '"K&M Properties" <kandm@sandandskirental.com>',
        to: params.to,
        subject: params.subject,
        text: params.text,
        html: params.html,
        attachments: params.attachments
    }

    let info = await Transporter.sendMail(options)

    return info
   
}

//Int ---> string
const DayOfWeekConverter = (day)=>{
    switch(day) {
        case 0:
            return 'Sunday'
        case 1:
            return 'Monday'
        case 2:
            return 'Tuesday'
        case 3:
            return 'Wednesday'
        case 4:
            return 'Thursday'
        case 5:
            return 'Friday'
        case 6:
            return 'Saturday'
        default:
            return '?'
    }
}


module.exports={CreateEmailer,SculptClientEmail,SendEmail}