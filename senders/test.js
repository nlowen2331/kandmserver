/*
    Take in a path, read file, get hash md5 hex representation of file
*/

const fs = require('fs')
const crypto = require('crypto')

const GetMD5Hash = async (path)=>{
    let HashedString = ''
    const readStream = await fs.createReadStream(path)
    const M5DHash = crypto.createHash('md5').setEncoding('hex')
    readStream.pipe(M5DHash)

    let ReturnValue = await new Promise((resolve,reject)=>{

        M5DHash.on('data',(chunk)=>{
            HashedString=`${HashedString}${chunk}`
        })

        M5DHash.on('finish', ()=>{
            resolve({OK: true, hash: HashedString})
        })
        M5DHash.on('error', (error)=>{
            return resolve({OK: false, error: error})
        })
    })

    return ReturnValue
    
}

GetMD5Hash('./fakefaile').then(res=>{
    console.log(res)
})

GetMD5Hash('./fakefile2').then(res=>{
    console.log(res)
})
