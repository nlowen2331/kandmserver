require('dotenv').config()
const StartUp = require('./routines/StartUp')
const SyncServerImagesWithS3 = require('./routines/SyncServerImagesWithS3')
const UpdateCalenders = require('./routines/UpdateCalenders')
const TimeConstants = require('./TimeConstants')
require('./DebugConfig')

//Begin running workers
StartUp().then(res=>{
    console.log(res)
    
    //Function wrappers
    const SyncServerImagesWithS3Function= ()=>{

        if(DEBUG_TIME_HAWK) {
            console.log('TIME HAWK: Sync Server with S3 Function called')
        }

        SyncServerImagesWithS3().then(res=>{
            console.log(res)
        }).catch(err=>{
            //Throw 
            console.log('(Routine Failure): Image syncing failed')
            throw err
        })
    }

    const UpdateCalendersFunction = () =>{

        if(DEBUG_TIME_HAWK) {
            console.log('TIME HAWK: Update Calenders Function called')
        }

        UpdateCalenders().then(res=>{
            console.log(res)
        }).catch(err=>{
            console.log('(Routine Failure): UpdateCalenders failed')
            console.log(err)
        })
    }

    //Function calls
    setImmediate(SyncServerImagesWithS3Function)
    setImmediate(UpdateCalendersFunction)
    setInterval(UpdateCalendersFunction,TimeConstants.minutes*2)

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: All functions created and called to finalize start up')
    }
    
}).catch(err=>{
    //Throw a start up error
    console.log('(Routine Failure): Start up failed')
    throw err
})



