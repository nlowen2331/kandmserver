/*
    Validate any ingoing SQL query that takes input directly
    from the HTTP request. This is a defense against SQL injection.

*/
const exp = {
    number: /^\d+$/,
    unitid:  /^\w{1,20}$/,
    s3Key: /^((IMG_\d{1,6})|((C|c)over)).((jpg)|(JPG)|(jpeg)|(JPEG)|(png)|(PNG))$/,
    password: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,20}$/,
    username: /^[\w]{5,20}$/,
    airbnbid: /^[\d]{8}$/,
    noBreakCharacters: /^[^\/|']*$/,
    nbc: /^[^\/|']*$/,

}

const Validator = (param,regex)=>{
    if(param==null||regex==null){
        return false
    }
    return regex.test(param)
}

module.exports={exp,Validator}

