
/*
    These functions/constants represent the actual queries being performed
    If syntax changes these must update
*/

const StoredProcedures = {
    select_units: {
        query: 'call select_units()',
    },
    select_messages: {
        query: 'call select_messages()',
    },
    select_all_ownerids: {
        query: 'call select_all_ownerids()',
    },
    select_unit_airbnbid: {
        query: 'call select_unit_airbnbid()',
    },
    select_units_without_folders: {
        query: 'call kandm.select_units_without_folders()',
    },
    select_unit_owners: {
        query: 'call kandm.select_unit_owners()',
    },
    get_picture_info_all_units: {
        query: 'call kandm.get_picture_info_all_units()',
    },
    select_all_unitpictures: {
        query: 'call kandm.select_all_unitpictures()'
    },
    insert_inquiry: {
        query: (json, unitid) => `call kandm.insert_inquiry('${json}',"${unitid}")`,
    },
    insert_inquiryV2: {
        query: (specs, priceInfo, unitid) => `call kandm.insert_inquiryV2('${specs}','${priceInfo}',"${unitid}")`,
    },
    insert_msg: {
        query: (fname, lname, email, phone, message, json) => (`call kandm.insert_msg('${fname}','${lname}','${email}','${phone}','${message}','${json}')`),
    },
    insert_calenders: {
        query: (unitid, calender) => (`call kandm.insert_calenders('${unitid}','${calender}')`)
    },
    insert_pictures: {
        query: (unitid, pictures) => (`call kandm.insert_pictures('${unitid}','${pictures}')`)
    },
    select_user: {
        query: (username) => (`call kandm.select_user('${username}')`)
    },
    insert_owner: {
        query: (username, password) => (`call kandm.insert_owner('${username}','${password}')`)
    },
    insert_min_max_prices: {
        query: (unitid, min, max) => (`call kandm.insert_min_max_prices('${unitid}','${min}','${max}')`)
    },
    select_inquiries_of_owner: {
        query: (owner) => (`call kandm.select_inquiries_of_owner('${owner}')`)
    },
    select_inquiries_with_status: {
        query: (owner, status) => (`call kandm.select_inquiries_with_status('${owner}',${status})`)
    },
    select_email_body: {
        query: (unitid, type) => (`call kandm.select_email_body('${unitid}','${type}')`)
    },
    insert_errormsg: {
        query: (msg, email, unitid) => (`call kandm.insert_errormsg('${msg}','${email}','${unitid}')`)
    },
    update_inq_status: {
        query: (inqid, status, ownerid) => (`call kandm.update_inq_status(${inqid},${status},${ownerid})`)
    },
    update_msg_status: {
        query: (msgid, statuses) => (`call kandm.update_msg_status(${msgid},'${statuses}')`)
    },
    update_inq_specs: {
        query: (inqid, json, ownerid) => (`call kandm.update_inq_specs(${inqid},'${json}',${ownerid})`)
    },
    post_inq_comment: {
        query: (inqid, comment, ownerid) => (`call kandm.post_inq_comment(${inqid},'${comment}',${ownerid})`)
    },
    select_calender_by_unitid: {
        query: (unitid) => (`call kandm.select_calender_by_unitid('${unitid}')`)
    },
    select_msg_statuses_by_id: {
        query: (msgid) => (`call kandm.select_msg_statuses_by_id(${msgid})`)
    },
    select_owner: {
        query: (ownerid) => (`call kandm.select_owner(${ownerid})`)
    },
    check_for_pictureless: {
        query: (ownerid) => (`call kandm.check_for_pictureless(${ownerid})`)
    },
    update_unit_pictures: {
        query: (unitid, JSON) => (`call kandm.update_unit_pictures('${unitid}','${JSON}')`)
    },
    verify_unit_owner: {
        query: (unitid, ownerid) => (`call kandm.verify_unit_owner('${unitid}',${ownerid})`)
    },
    verify_unit_owner2: {
        query: (unitid, ownerid) => (`call kandm.verify_unit_owner2('${unitid}',${ownerid})`)
    },
    select_unit_pictures: {
        query: (unitid) => (`call kandm.select_unit_pictures('${unitid}')`)
    },
    select_specs_for_unit: {
        query: (unitid) => (`call kandm.select_specs_for_unit('${unitid}')`)
    },
    update_specs_for_unit: {
        query: (unitid,JSON,ownerid) => (`call kandm.update_specs_for_unit('${unitid}','${JSON}',${ownerid})`)
    },
    update_calender_for_unit: {
        query: (unitid,JSON,ownerid) => (`call kandm.update_calender_for_unit('${unitid}','${JSON}',${ownerid})`)
    },
    update_pricem_for_unit: {
        query: (unitid,JSON,ownerid) => (`call kandm.update_pricem_for_unit('${unitid}','${JSON}',${ownerid})`)
    },
    select_inq_by_id: {
        query: (inqid) => (`call kandm.select_inq_by_id(${inqid})`)
    },
    select_unit_by_id: {
        query: (unitid) => (`call kandm.select_unit_by_id('${unitid}')`)
    },
    select_pricem_by_unitid: {
        query: (unitid) => (`call kandm.select_pricem_by_unitid('${unitid}')`)
    },
    accept_inquiry: {
        query: (inqid,ownerid,specs) => (`call kandm.accept_inquiry(${inqid},${ownerid},'${specs}')`)
    },
    select_single_unit_bnbid: {
        query: (unitid) => (`call kandm.select_single_unit_bnbid('${unitid}')`)
    },
    update_unit_name: {
        query: (unitid,unitname) => (`call kandm.update_unit_name('${unitid}','${unitname}')`)
    },
    update_unit_desc: {
        query: (unitid,unitdesc) => (`call kandm.update_unit_desc('${unitid}','${unitdesc}')`)
    }
}

module.exports.StoredProcedures = StoredProcedures