
const fs = require('fs').promises
const _fs = require('fs')
const dayjs = require('dayjs')
const { GetBucketListing, GetBucketObject } = require('../fetchers/S3Fetcher')
const LogProcedure = require('../loggers/ErrorReporter')
const { UpdateUnitPictures, UseProcedure } = require('./DBWorker')
const crypto = require('crypto')
const { StoredProcedures } = require('./DBStoredProcs')

//Get the S3 Listing for kandmpictures bucket
const GetS3Listing = async (bucket_name) => {
    let BucketObjects

    //Attempt to get objects from S3
    BucketObjects = await GetBucketListing(bucket_name)
    if (DEBUG_ALL) {
        console.log(`Logging Bucket Listing from S3...`)
        console.log(BucketObjects)
    }

    let Contents = BucketObjects.Contents

    if (Contents == null) {
        throw new Error('The S3 response was unfamiliar')
    }
    return (Contents)
}

//Attempt to sync the supplied list of Bucket files with server files
//This is a one way sync, files with no counterparts in the S3 Bucket will
//not be affected, S3 objects not present will be downloaded
const SyncS3Contents = async (supplied_list,bucket_name) => {

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: SyncS3Contents called')
    }

    //If there was a supplied array, use it, 
    //else pull from S3 Bucket

    let Contents

    if (supplied_list == null || supplied_list.length < 1) {

        Contents = await GetS3Listing(bucket_name)

        if(DEBUG_SYNC) {
            console.log('Logging output of S3 Bucket for file sync')
            console.log(Contents)
        }
    }
    else {
        Contents = supplied_list
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Bucket contents decided')
    }

    //Test all contents with  regular expression, any files with improper names
    //will not be synced

    /*
        MISSING CODE

    */

    let use_folder
    
    //Map bucket name to the corresponding server directory
    switch(bucket_name) {
        case process.env.PICTURE_BUCKET:
            use_folder=process.env.PICTURE_FOLDER
            break;
        case process.env.TEMPLATE_BUCKET:
            use_folder=process.env.TEMPLATE_FOLDER
            break;
        default:
            throw new Error(`Could not find corresponding server directory for S3 Bucket: ${bucket_name}`) 
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Buckets determined from env variables')
    }

    //If use_folder remains null throw an error
    if(use_folder==null) {
        throw new Error(`No folder has been specified to sync S3 data for ${bucket_name}, check your environment variables`)
    }

    //CheckedFiles represents an array of newly formatted JSON Objects that
    // reflects the S3 inconsistincies with server

    let CheckedFiles = await Promise.all(Contents.map(bucket_item => (isFileToDate(bucket_item,use_folder))))
    if (DEBUG_SYNC) {
        console.log('Logging CheckedFiles')
        console.log(CheckedFiles)
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Checked files determined')
    }

    //If the function throws an error, something unforseen occured

    let ProblemFiles = [] //CheckedFiles with inconsistencies
    let NonProblemFiles = [] //CheckFiles with no inconsistencies, these will be concatenated with 
    //fixed problem files to be sent to DB
    let ThrewErrorFiles = [] //CheckFiles that threw error when checked for inconsistencies

    if(DEBUG_SYNC) {
        console.log('S3Sync: Parsing checked files')
    }

    CheckedFiles.forEach(s3_file => {
        //Toss out folders (size 0 objects)
        if(s3_file.isFolder) {
            return
        }
        else if (s3_file.threwError) {
            ThrewErrorFiles.push(s3_file)
        }
        else if (!s3_file.folder_exists || !s3_file.file_exists || !s3_file.file_equality) {
            ProblemFiles.push(s3_file)
        }
        else {
            NonProblemFiles.push(s3_file)
        }
    })

    if(DEBUG_SYNC||DEBUG_TIME_HAWK) {
        if(DEBUG_TIME_HAWK){
            console.log('TIME HAWK:')
        }
        console.log('S3Sync: Checked files parsed and seperated')
        console.log(`ThrewErrorFiles Length: ${ThrewErrorFiles.length}`)
        console.log(`ProblemFiles Length: ${ProblemFiles.length}`)
        console.log(`NonProblemFiles Length: ${NonProblemFiles.length}`)
    }

    //If there were no inconsistent files resolve

    if (ProblemFiles.length < 1 && ThrewErrorFiles.length < 1) {
        if (DEBUG_SYNC) {
            console.log(`Will resolve successfully with fixes`)
            console.log({ OK: true, data: NonProblemFiles })
        }
        return ({ OK: true, data: NonProblemFiles })
    }

    //Have Error Reporter log all files that threw an error
    try {
        await LogProcedure({ data: { ErrorFiles: ThrewErrorFiles } }, 'InconsistentFiles')
    }
    catch (err) {
        console.log('There was an error creating the Inconsistent Files Report')
        console.log(err)
    }

    //If any errors were thrown
    if (ThrewErrorFiles.length > 0) {
        throw new Error('An error(s) occured on the to-date checks, refer to Inconsistent Files Report')
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Begin auditing files')
    }

    //Attempt to fix all inconsistent files

    let AuditedFiles //ProblemFiles that have had fixes attempted

    if(DEBUG_SYNC) {
        console.log('S3Sync: Attempting to fix problem files')
    }

    AuditedFiles = await Promise.all(ProblemFiles.map(s3_file => fixProblemFile(s3_file,use_folder,bucket_name)))

    if (DEBUG_SYNC) {
        console.log('Logging Audited Files')
        console.log(AuditedFiles)
    }

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: All files audited')
    }

    let UnFixedFiles = [] //AuditedFiles that remain inconsistent after attempt
    let FixedFiles = [] //AuditedFiles that were fixed by audit
    ThrewErrorFiles = [] //AuditedFiles that threw errors

    AuditedFiles.forEach(audit_file => {
        if (audit_file.threwError) {
            ThrewErrorFiles.push(audit_file)
        }
        else if (!audit_file.folder_exists || !audit_file.file_exists || !audit_file.file_equality) {
            UnFixedFiles.push(audit_file)
        }
        else {
            FixedFiles.push(audit_file)
        }
    })

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: Audited files seperated')
    }

    //Have Error Reporter log all files that threw errors
    if (ThrewErrorFiles.length > 0) {
        try {
            await LogProcedure({ data: { ThrewErrorFiles: ThrewErrorFiles } }, 'UnFixedFiles')
        }
        catch (err) {
            console.log('There was an error creating the UnFixed Files Report')
            console.log(err)
        }
    }

    //If any errors were thrown, throw error
    if (ThrewErrorFiles.length > 0) {
        throw new Error('An error(s) occured on attempt to fix files, refer to the UnFixed Files Report')
    }

    //If some files went unfixed, return all problems
    if (UnFixedFiles.length > 0) {
        return { OK: false, data: [...NonProblemFiles, ...UnFixedFiles, ...FixedFiles] }
    }

    if (DEBUG_SYNC) {
        console.log(`Will resolve successfully with fixes`)
        console.log({ OK: true, data: [...NonProblemFiles, ...FixedFiles] })
    }

    //All problems solved, return OK

    return ({ OK: true, data: [...NonProblemFiles, ...FixedFiles] })

}

//Take in a single bucket item (originating from the S3), output a bucket_item that records if
//it is not synced with server storage, throws out improperly named files
//Also takes in a directory (must be a folder in the root directory)

const isFileToDate = async (bucket_item,dir) => {

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: isFileToDate Called')
    }

    if (bucket_item == null || bucket_item.Key == null || bucket_item.ETag == null) {
        return ({ threwError: true, message: 'Bucket item was either null or missing required info' })
    }

    //If size is 0 it is just a folder so toss it but don't throw error
    if(bucket_item.Size===0) {
        return ({threwError: false, isFolder: true, message: 'This is just a representation of the folder as a whole'})
    }

    const key = bucket_item.Key

    //Check if Key is valid to be stored on server

    //This routine was run before start up
    if(!global.UnitIdArrayIsLoaded || global.UnitIdArray.length<1) {
        throw new Error('Cannot determine legal S3 Keys because (global) UnitIdArray has not been extansiated/is empty. This routine '
            + 'may have been run before `StartUp` routine')
    }

    //Key must contain exactly 1 '/' to differentiate the unit and filename

    let illegalDir = (key.match(/\//g) || []).length !== 1

    //Properties of the key

    const folderEnd = key.indexOf('/')
    const folderName = key.substring(0, folderEnd)
    const fileName = key.substring(folderEnd + 1)

    if(DEBUG_SYNC) {
        console.log('Logging attributes of bucket_item key')
        console.log({folderEnd: folderEnd, folderName: folderName, fileName: fileName})
    }

    //Filename must not be null
    if(fileName==null || fileName==="") {
        return ({ threwError: true, message: 'Bucket item was missing a fileName'})
    }


    //The folder name of they key must exist in records

    let noKnownUnit = !global.UnitIdArray.includes(folderName)

    let statement = {
        Key: key,
        ETag: bucket_item.ETag,
        folderName: folderName,
        fileName: fileName,
        folder_exists: false,
        file_exists: false,
        file_equality: false,
    }

    if(illegalDir) {
        return {...statement, threwError: true, message: 'Error: This key did not contain exactly 1 `/`'}
    }

    if(noKnownUnit) {
        return {...statement, threwError: true, message: 'Error: This unit does not exist'}
    }

    try {

        //Check if the folder exists
        try {
            await fs.access(`${dir}/${folderName}`)
        }
        catch (err) {
            return statement
        }

        if(DEBUG_TIME_HAWK) {
            console.log('TIME HAWK: Checked if folder exists')
        }

        statement.folder_exists = true

        //Check if file exists
        try {
            await fs.access(`${dir}/${folderName}/${fileName}`)
        }
        catch (err) {
            return statement
        }

        if(DEBUG_TIME_HAWK) {
            console.log('TIME HAWK: Checked if file exists')
        }

        statement.file_exists = true

        /*Use function 'getMD5Hash()'
            Takes in a path as an arguement and returns a MD5 hex-digested string representation of the file

        */

        let HashRes = await GetMD5Hash(`${dir}/${folderName}/${fileName}`)
        
        if(DEBUG_HASH){
            console.log('Hashed File (base64)')
            console.log(HashRes)
            console.log('Bucket Item ETag')
            console.log(bucket_item.ETag)
        }

        if (HashRes.OK&&HashRes.hash !== bucket_item.ETag) {
            return statement
        }
        
        statement.file_equality = true

        if(DEBUG_TIME_HAWK) {
            console.log('TIME HAWK: Returning no issues')
            
        }

        //No issues
        return (statement)
    }
    catch (err) {
        return ({ ...statement, threwError: true, message: err.toString() })
    }

}

//Evaluate given bucket_object, attempt to fix any problem specified by its attributes

const fixProblemFile = async (problem_file,dir,bucket_name) => {

    if(DEBUG_TIME_HAWK) {
        console.log('TIME HAWK: fixProblemFile called')
    }

    if (problem_file == null || problem_file.Key == null) {
        throw new Error('The bucket item provided was either null or was missing required information')
    }

    let statement = { ...problem_file }

    try {

        //check if missing folder
        if (!statement.folder_exists) {
            try {
                await fs.mkdir(`${dir}/${statement.folderName}`, { recursive: true })
            }
            catch (err) {
                return { ...statement, threwError: 'true', message: err.toString() }
            }
            statement.folder_exists = true
        }

        //check if missing file or dates did not match on audit
        if (!statement.file_exists || !statement.file_equality) {
            let S3File
            try {
                S3File = await GetBucketObject(statement.Key, bucket_name)
                if (DEBUG_ALL) {
                    console.log('Logging Downloaded S3 Files')
                    console.log(S3File)
                }
            }
            catch (err) {
                return { ...statement, threwError: 'true', message: err.toString() }
            }

            //Write S3 files to server directory
            try {
                await fs.writeFile(`${dir}/${statement.folderName}/${statement.fileName}`, S3File.Body, { encoding: 'binary' })
            }

            catch (err) {
                return statement = { ...statement, threwError: 'true', message: err.toString() }
            }

            statement.file_exists = true
            //This is assumed true
            statement.file_equality = true

        }


    }

    catch (err) {
        return ({ ...statement, threwError: true, message: err.toString() })
    }

    //No errors

    return statement
}

/*
    Take in array of JSON objects like
    {
        fileName: 'value',
        folderName: 'value
    }

    Return --> [ {unitid: folderName, data: [ {picture_id: fileName},{picture_id: fileName} ] } ]

*/
const DBPictureDataFormatter = async (json) => {

    DB_Picture_Data = []

    json.forEach(synced_file => {
        const folder = synced_file.folderName
        const fileName = synced_file.fileName
        let index = -1
        //Find index of the correct JSON object for the synced_file's contents
        if (DB_Picture_Data.length > 0) {

            if (DEBUG_SYNC) {
                console.log('Logging DB_Picture_Data')
                console.log(DB_Picture_Data)
            }

            DB_Picture_Data.forEach((unit, unit_index) => {

                if (folder === unit.unitid) {
                    index = unit_index
                }
            })
        }
        //If no JSON object found for the synced_file, create one and set index
        if (index === -1) {
            DB_Picture_Data.push({ unitid: folder, data: [] })
            index = DB_Picture_Data.length - 1
        }
        //Push that synced_file to JSON object
        const matching_dataset = DB_Picture_Data[index].data
        matching_dataset.push({
            picture_id: fileName,
            isHidden: false,
            isCover: (fileName.toLowerCase() === 'cover.jpg' ? true : false)
        })
    })

    return DB_Picture_Data

}

//Take in an array of JSON objects, format them, then update DB with the results

const SyncDBWithServerImages = async (ready_files) => {

    let DB_Picture_Data = await DBPictureDataFormatter(ready_files)
    
    //Pass each query to UpdateUnitPictures and compile an array of results
    let promises = await Promise.all(DB_Picture_Data.map(query => (UpdateUnitPictures(query))))

    return ('Successfully updated the DB')
}

//Delete all files on server with no S3 counterpart (name-wise)
//takes in an S3 listing
//This function in practicality catches files that were deleted from the S3
//but still have counterparts on the server
const RemoveOutdatedFiles = async (S3Listing) => {
    //for each entry in the images folder
    //see if there is an equivalent in the S3 Listing
    //if there is no S3 
    if(S3Listing==null) {
        S3Listing = await GetS3Listing(process.env.PICTURE_BUCKET)
    }

    let Pictures = await UseProcedure(StoredProcedures.select_all_unitpictures,null,true)

    //Compile a list of each unitid/picture in the form of a path
    let Paths = []

    Pictures.forEach(row=>{
        let row_pictures = row.pictures
        let row_unitid = row.unitid

        //no picture data exists for a unit
        if(row_pictures==null){
            return
        }

        row_pictures=JSON.parse(row_pictures)
        row_pictures.forEach(row_pic=>{

            //This would indicate bad data in the DB
            if(row_pic.picture_id==null) {
                return
            }

            Paths.push(`${row_unitid}/${row_pic.picture_id}`)
        })
    })

    //Confirm that all keys found in DB have S3 counterparts
    let BadPaths = []

    Paths.forEach(path=>{
        let existsInS3=false
        existsInS3= !S3Listing.every(listing=>{
            let key = listing.Key
            if(path===key) {
                return false
            }
            else{
                return true
            }
        })
        //Key/Path doesn't exist in S3
        if(!existsInS3){
            BadPaths.push(path)
        }
    })

    //No conflicts
    if(BadPaths.length<1) {
        return(`No outdated files exist on server`)
    }

    //Attempt to delete files
    let promises = await Promise.all(BadPaths.map( async path=>{
        let error= 'none'
        await fs.unlink(`./images/${path}`).catch(err=>{
            error=err
        })
        //If thereis an error, or the file has already been deleted
        if(error!=='none'&&error.code!=='ENOENT') {
            return ({error: true,message: error,...path})
        }
        else{
            return({error: false,...path})
        }
    }))

    //Check if any errors occured

    let ErrorPromises= []
    let SuccessPromises=[]

    //Filter out successes
    promises.forEach(promise=>{
        if(promise.error){
            ErrorPromises.push({...promise})
        }
        else{
            SuccessPromises.push({...promise})
        }
    })

    //Log errors and successes and end program
    if (ErrorPromises.length > 0) {
        try {
            await LogProcedure({ data: [ ...ErrorPromises ] }, 'OutdatedFileErrors')
        }
        catch (err) {
            console.log('There was an error creating the Outdated File Error Report')
            console.log(err)
        }
    }

    if (SuccessPromises.length > 0) {
        try {
            await LogProcedure({ data: [ ...SuccessPromises ] }, 'DeletedFiles')
        }
        catch (err) {
            console.log('There was an error creating the Deleted Files Report')
            console.log(err)
        }
    }

    if(ErrorPromises.length>0) {
        return('Error deleting some outdated files, check OutdatedFileErrors')
    }

    //Successfully deleted files
    return('Successfully deleted all outdated files, check DeletedFiles for more info')


}

/*
    Take in a path, read file, get hash md5 hex representation of file
*/
const GetMD5Hash = async (path)=>{
    let HashedString = ''
    const readStream = _fs.createReadStream(path)
    const M5DHash = crypto.createHash('md5').setEncoding('hex')
    readStream.pipe(M5DHash)

    let ReturnValue = await new Promise((resolve,reject)=>{

        M5DHash.on('data',(chunk)=>{
            HashedString=`${HashedString}${chunk}`
        })

        M5DHash.on('finish', ()=>{
            resolve({OK: true, hash: HashedString})
        })
        M5DHash.on('error', (error)=>{
            return resolve({OK: false, error: error})
        })
    })

    return ReturnValue
    
}

module.exports = { SyncS3Contents, SyncDBWithServerImages, GetS3Listing, RemoveOutdatedFiles }


