const mysql = require('mysql')
const { update_unit_pictures } = require('../config')
const { StoredProcedures } = require('./DBStoredProcs')
require('dotenv').config()

let WorkerRunning = false
let pool

//These functions are needed so that the legacy routes can hook into the DBWorker
const _GetPool = () => {return pool}
const _GetWorkerRunning = () => {return WorkerRunning}

const CreatePool = async () => {

    if (process.env.DB_PASSWORD == null) {
        throw new Error('Could not determine DB_PASSWORD from Environment Variables')
    }
    if (process.env.DB_USER == null) {
        throw new Error('Could not determine DB_USER from Environment Variables')
    }
    if (process.env.DB_ADDRESS == null) {
        throw new Error('Could not determine DB_ADDRESS from Environment Variables')
    }
    if (process.env.DB_SCHEMA == null) {
        throw new Error('Could not determine DB_SCHEMA from Environment Variables')
    }

    pool = mysql.createPool({
        host: process.env.DB_ADDRESS,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_SCHEMA
    })

    WorkerRunning = true

    return ('DB Connection Pool was initialized')
}



/*Update existing unit pictures for all specified
    units in the param
    param is in Picture Data Formatter Form:
    [ {unitid: folderName, data: [ {picture_id: fileName},{picture_id: fileName} ] } ]
*/
const UpdateUnitPictures = async (in_unit) => {

    if (!WorkerRunning) {
        throw new Error('Connection Pool has not been established, cannot execute SQL queries')
    }

    let Query = []
    const unitid = in_unit.unitid
    const picture_data = in_unit.data

    //Get pictures for unitid

    let RowData = await UseProcedure(StoredProcedures.select_unit_pictures, unitid)

    if (DEBUG_ALL) {
        console.log('Logging RowData')
        console.log(RowData)
    }

    RowData = RowData[0]

    if (RowData.length < 1) {
        throw new Error(`The unitid ${unitid} did not match any existing records`)
    }

    const DBUnit = RowData[0]
    let db_unit_picture_data = DBUnit.pictures

    //If no data exists use input JSON
    if (db_unit_picture_data == null) {
        Query = picture_data
    }

    //If data exists for the given unit, append the new input data with
    //existing data
    else {

        db_unit_picture_data = JSON.parse(db_unit_picture_data)
        //iterate through both data sets
        picture_data.forEach(picture => {
            db_unit_picture_data.every(db_picture => {
                //If matching picture ids exist then append
                if (db_picture.picture_id === picture.picture_id) {
                    if (db_picture.isHidden != null) {
                        picture.isHidden = db_picture.isHidden
                    }
                    if (db_picture.isCover != null) {
                        picture.isCover = db_picture.isCover
                    }

                    return false
                }

                return true
            })

            Query.push(picture)

        })


    }

    let UpdateResponse = await UseProcedure(StoredProcedures.update_unit_pictures, [unitid, JSON.stringify(Query)])

    if (UpdateResponse.affectedRows === 0) {
        return (`Query was successful, but no rows were updated`)
    }
    else {
        return (`Query was successful`)
    }
}

//Takes in a query and inputs for the query (if function), validates the input and returns the results
//inputs is an array of ordered arguements for the query function
//If resultsOnly is true, the res will only include table of results
const UseProcedure = async (query, inputs,resultsOnly) => {

    //The actual query is nested in the query JSON
    query = query.query

    //Each input should be cleansed of break characters

    /*
        clean inputs
    */

    if (typeof query === 'function') {
        if(typeof inputs === 'object') {
            query = query(...inputs)
        }
        else{
            query = query(inputs)
        }
        
    }

    if (DEBUG_ALL||DEBUG_DB_WORKER) {
        console.log('Logging to-use query')
        console.log(query)
    }

    let res = await new Promise((resolve, reject) => {
        pool.query(query, (err, res) => {

            if (DEBUG_DB_WORKER_RES) {
                console.log('Logging response')
                console.log(err)
                console.log(res)
            }

            if (err) {
                reject(err)
            }
            else {
                if(resultsOnly) {
                    resolve(res[0])
                }
                else{
                    resolve(res)
                }     
            }
        })
    })

    return res

}

module.exports = { UpdateUnitPictures, CreatePool, UseProcedure, _GetPool,_GetWorkerRunning }
