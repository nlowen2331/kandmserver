const define = (name, value) =>{
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

define("NO_ERROR", 0);
define("NO_CONNECTION", 1);
define("PROCEDURE_ERROR", 2);
define("MISSING_ID", 3);
define("AIRBNB_API", 4);
define("NO_MATCHING_USER", 5);
define("INCORRECT_PASSWORD", 6);
define("IMPROPER_REQUEST", 7);
define("JWT_SIGNING_ERROR",8);
define("JWT_VERIFY_ERROR",9);
define("AUTH_DATA_ODDITY",10)
define("NO_CHANGES",11)
define("YOUNG_SERVER",12)
define("UNKNOWN_ERROR",13)
define("UNAUTHORIZED_REQUEST",14)
define("DB_ERROR",15)
define("NO_MATCHING_DATA", 16);
define("IO_ERROR",17)