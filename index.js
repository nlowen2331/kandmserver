
const express = require('express')
const TestRoute = require('./routes/TestRoute')
const GetUnitsRoute = require('./routes/GetUnitsRoute')
const InquiryRoute = require('./routes/PostInqRoute')
const MessageRoute = require('./routes/PostMessageRoute')
const AuthRoute = require('./routes/AuthenticationRoute')
const GetCalenderRoute = require('./routes/GetCalenderRoute')
const LoginRoute = require('./routes/LoginRoute')
const GetOwnerInquiriesRoute = require('./routes/GetOwnerInquiriesRoute')
const UpdateCalenders = require('./UpdateCalenders')
const UpdateStatusRoute = require('./routes/UpdateInqStatusRoute')
const UpdateMessageStatusRoute = require('./routes/UpdateMsgStatusRoute')
const UpdateSpecsRoute = require('./routes/UpdateInqSpecsRoute')
const PostCommentRoute = require('./routes/PostCommentRoute')
const SelectCalendersRoute = require('./routes/GetCalenderByUnit')
const SelectMessagesRoute = require('./routes/GetMessages')
const SelectOwnerRoute = require('./routes/GetOwnerRoute')
const {getIdsLoaded,getOwnerIds} = require('./GetOwnerIds')
const UpdatePicturesRoute = require('./routes/UpdatePicturesRoute')
const GetUnitPictureFromS3Route = require('./routes/GetUnitPictureFromS3Route')
const { SyncS3Contents } = require('./writers/ImageSynchronizer')
require('dotenv').config()

//set up server

const server = express()

const allowCrossDomain = (req, res, next)=> {
    res.header('Access-Control-Allow-Origin', "*")
    res.header('Access-Control-Allow-Methods', 'GET, POST,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    next()
}

server.use(allowCrossDomain)

server.use(express.json())

const port = process.env.PORT||4000

server.listen(port)

console.log("Server listening on port " + port)

//routes

server.use('/Ping',TestRoute)
server.use('/GetUnits',GetUnitsRoute)
server.use('/Inquiry',InquiryRoute)
server.use('/Message',MessageRoute)
server.use('/Authenticate',AuthRoute)
//server.use('/GetCalender',GetCalenderRoute)
server.use('/Login',LoginRoute)
server.use('/GetInquiryByOwner',GetOwnerInquiriesRoute)
server.use('/UpdateStatus',UpdateStatusRoute)
server.use('/UpdateSpecs',UpdateSpecsRoute)
server.use('/PostOwnerComment',PostCommentRoute)
server.use('/SelectCalenderByUnit',SelectCalendersRoute)
server.use('/GetMessages',SelectMessagesRoute)
server.use('/UpdateMsgStatus',UpdateMessageStatusRoute)
server.use('/GetOwner',SelectOwnerRoute)
server.use('/UpdatePictures',UpdatePicturesRoute)
server.use('/GetS3Picture',GetUnitPictureFromS3Route)

//Start updating calenders
//setTimeout(()=>UpdateCalenders(),1000*2)
//setInterval(()=>UpdateCalenders(),1000*60)

//Check S3 Buckets

/*
setTimeout(()=>{
    UpdatePictures().then(res=>{
        console.log(res)
    }).catch(err=>{
        console.log(err)
    })
},1000*7)
*/

SyncS3Contents().then(res=>{
    console.log(res)
}).catch(err=>{
    console.log(err)
})

