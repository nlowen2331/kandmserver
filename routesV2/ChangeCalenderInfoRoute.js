const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')
const dayjs = require('dayjs')
const currency = require('currency.js')

/*
    Takes in post request with 
    [{ unitid, calenderChangeStack}]

    Copied code template from ChangePictureInfoRoute
*/

let lock = []

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to update calender for a unit')

    if (req.headers["content-type"] !== 'application/json') {
        //Unsupported Media Type sent if not JSON
        return res.sendStatus(415)
    }

    //Verify unit owner
    let DBRes = await UseProcedure(StoredProcedures.verify_unit_owner2, [req.body.unitid, req.payload.id], true)


    //Could not verify unit ownership
    if (DBRes.length < 1) {
        return res.sendStatus(403)
    }

    let Unit = DBRes[0].unitid
    
    //Get specifications for given unit
    let Calender_Res = await UseProcedure(StoredProcedures.select_calender_by_unitid,Unit,true)

    //If no data, send server error
    if(Calender_Res.length<1) {
        return res.sendStatus(500)
    }

    //To avoid data loss, do not allow this process to run twice for the same unitid
    if (lock.includes(Unit)) {
        //Service unavailable
        return res.sendStatus(503)
    }

    //Set lock on this unitid
    lock.push(Unit)

    //Get the json
    let Calender
    try{
        Calender=JSON.parse(Calender_Res[0].calender).days
    }
    catch(err) {
        return res.status(500).send({Errors: [`Could not parse DB Records`]})
    }

    //Get Changes
    let ChangesArray=req.body.changes

    //for each change manipulate the calender data
    let Errors=[]

    //For each change
    ChangesArray.forEach(change => {
        //evName of all events is calenderEvent
        if(change.evName==='calenderEvent'){
            //Iterate through each day (YYYY-MM-DD) in daysAffected
            change.daysAffected.forEach(affected_day=>{
                //Find matching date in db
                let toUpdate = Calender.find(db_day=>dayjs(db_day.date).isSame(dayjs(affected_day),'day'))
                //Push error if no matching date was found
                if(toUpdate==null) {
                    Errors.push(`Could not find matching db date for ${affected_day}`)
                }
                //Update all values that the nested 'value' obj specifies
                else{
                    //Update whether or not airbnb pull should override price
                    switch(change.value.priceSyncSetting){
                        case 'sync':
                            toUpdate.syncPrice=true
                            break;
                        case 'nosync':
                            toUpdate.syncPrice=false
                            break;
                        case 'none':
                            break;
                        default:
                            Errors.push(`Unknown price sync value given ${change.value.priceSyncSetting}`)
                    }
                    //Update price
                    if(change.value.priceChange) {
                        let safePrice = currency(change.value.price)
                        if(safePrice.value>0){
                            toUpdate.price=safePrice
                        }
                        else{
                            Errors.push(`Price for day ${change.date} was either set to 0 or invalid`)
                        }
                    }
                }
            })
        }
        else{
            Errors.push(`Improper evName ${change.evName}, must be 'calenderEvent'`)
        }
    })

    //Do not update the DB upon errors, instead send back an improper request
    //along with the Error array
    if(Errors.length>0) {
        UnLock(Unit)
       return res.status(409).send({Errors: Errors})
    }

    //Accept the new array and update the DB with the new information
    let InsertResponse
    let DB_Error='none'
    try{
        InsertResponse = await UseProcedure(StoredProcedures.update_calender_for_unit,[Unit,JSON.stringify({days: Calender}),req.payload.id])
    }
    catch(err) {
        DB_Error=err
    }

    if(DB_Error!=='none'){
        return res.status(500).send({Errors: ['DB Error']})
    }

    console.log(`Successfully inserted new calender for unit: ${Unit}`)

    if(InsertResponse.affectedRows<1) {
        UnLock(Unit)
        return res.status(200).send({msg: 'No rows affected'})
    }
    else{
        UnLock(Unit)
        return res.status(200).send({msg: `Data was affected`})
    }

})

const UnLock=(unitid)=>{
    let lockIndex = lock.findIndex(lock=>lock===unitid)
    lock = lock.slice(lockIndex,lockIndex)
}

module.exports = router
