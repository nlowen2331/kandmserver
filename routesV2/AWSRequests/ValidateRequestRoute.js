const express = require('express')
const router = express.Router()
const openssl = require('openssl-wrapper')
const request = require('request')
const pki = require('node-forge').pki

//Check if the post request is from AWS
router.post('*', async (req, res, next) => {

    let headers = req.headers

    if (headers['user-agent'] !== 'Amazon Simple Notification Service Agent' &&
        headers['User-Agent'] !== 'Amazon Simple Notification Service Agent') {
       return next()
    }

    if(DEBUG_ROUTES) {
        console.log('AWS SNS Request is being processed')
    }

    //Find if signature is valid to certify its contents

    let isValidSig = false

    try {
        isValidSig = await ValidateSignature(req)
    }
    catch (err) {
        console.log(err)
        isValidSig = false
    }

    //If invalid then send forbidden and return
    if (!isValidSig) {

        if(DEBUG_ROUTES) {
            console.log('Signature is invalid, sending a 403: Forbidden to sender')
        }

        res.sendStatus(403)
        return
    }

    //Signature was valid

    req.SignatureVerfiedAwsRequest=isValidSig

    if(DEBUG_ROUTES) {
        console.log('Signature was verified! The server trusts the information')
    }

    return next()
});

//This function takes in an array of claims and outputs a
//newline-delimited string 
const CreateNewLineDelimitedString = async (claimArray) => {
    //init return string
    let ReturnString = ``

    //regexp to convert escapees
    const rx = /\\\\([0-9A-Fa-n]+)/;

    //parse through claims
    claimArray.forEach(claim => {
        //get all json keys
        let keys = Object.keys(claim)
        //append key
        ReturnString += (keys[0] + `\n`)
        //append value
        let value = JSON.stringify(claim[keys[0]])

        ReturnString += (value + `\n`)
    })
    return ReturnString
}

//Takes in the request body and validates the message contents

const ValidateSignature = async (req) => {

    let error = 'none'

    console.log('ValidateSignature method was called')
    console.log('This functionality is not complete, the signature will be blindly validated')

    return true
    //Find out if its a Subscription Confirmation
    /*
    if (headers['x-amz-sns-message-type'] === 'SubscriptionConfirmation') {

        if (DEBUG_ROUTES) {
            console.log(`Subscription request from AWS`)
        }

        //Make list of claims for calculating sig
        const claims = [
            { Message: body.Message },
            { MessageId: body.MessageId },
            { SubscribeURL: body.SubscribeURL },
            { Timestamp: body.Timestamp },
            { Token: body.Token },
            { TopicArn: body.TopicArn },
            { Type: body.Type }
        ]

        //Decode Signature from Base64
        let buffer = Buffer.from(body.Signature,'base64')
        let DecodedSignature = buffer.toString('ascii')

        if (DEBUG_ROUTES) {
            console.log(`Decoded Signature`)
            console.log(DecodedSignature)
        }

        if (DEBUG_ROUTES) {
            console.log(`Claims for creating sig`)
            console.log(claims)
        }

        //Create the newline delimited string

        const StringToTest = await CreateNewLineDelimitedString(claims)

        if (DEBUG_ROUTES) {
            console.log(`The newline delimited string`)
            console.log(StringToTest)
        }

        //Send GET request to the SigningCertURL for X509 Certificate
        let X509Cert = await new Promise((resolve, reject) => {
            request(body.SigningCertURL, (err, res, body) => {
                if (err) {
                    reject(err)
                }
                resolve({ res: res, body: body })
            })
        }).catch(err => {
            error = err
        })

        if (error !== 'none') {
            console.log(err)
            return
        }

        if (DEBUG_ROUTES) {
            console.log(`X509 Certificate`)
            console.log('Body')
            console.log(X509Cert.body)
        }

        //Extract the public key from the certificate

        let DecryptedCertificate = pki.certificateFromPem(X509Cert.body)
        
        if (DEBUG_ROUTES) {
            console.log(`Public Key`)
            console.log(DecryptedCertificate)
        }

        let PublicKey = DecryptedCertificate.publicKey

        return

        //res.sendStatus(200)
    }
    */
}

module.exports=router