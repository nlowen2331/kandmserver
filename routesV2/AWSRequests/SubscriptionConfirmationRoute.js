const express = require('express')
const router = express.Router()
const request = require('request')

router.post('*',(req,res,next)=>{

    let headers = req.headers

    //If this was not a verified request or the headers don't match then pass 
    if(!req.SignatureVerfiedAwsRequest || headers['x-amz-sns-message-type'] !== 'SubscriptionConfirmation') {
        return next()
    }

    if (DEBUG_ROUTES) {
        console.log('Subscription Confirmation')
        console.log(body.Message)
    }

    let SubscribeRes = await (new Promise((resolve, reject) => {
        request(body.SubscribeURL, (err, res, body) => {
            if (err) {
                reject({ OK: false, error: err })
                return
            }
            else {
                resolve({ OK: true, res: res, body: body })
            }
        })
    })).catch(err => {
        return ({ OK: false, error: err })
    })

    if (DEBUG_ROUTES) {
        console.log('Attemtped to request Subscribe URL')
        console.log(`Success? ${SubscribeRes.OK}`)
        if(SubscribeRes.error!=null) {
            console.log(`Error: ${SubscribeRes.error}`)
        }
    }

    //success
    if (SubscribeRes.OK) {
        console.log(`Successfully subscribed to AWS Topic`)
        console.log(`Response: ${SubscribeRes.body}`)
        return res.sendStatus(200)
    }

    //failure
    console.log(`Failed to subscribe to AWS Topic`)
    return res.sendStatus(500)

})

module.exports=router