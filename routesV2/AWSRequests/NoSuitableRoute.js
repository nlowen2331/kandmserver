const express = require('express')
const router = express.Router()

router.post('*',(req,res,next)=>{

    let headers = req.headers

    //If no signature
    if(!req.SignatureVerfiedAwsRequest) {
        return next()
    }

    if (DEBUG_ROUTES) {
        console.log('Server had no suitable response for this message type')
        console.log('Sending 404 Error')
    }

    return res.sendStatus(404)
    
})

module.exports=router