const express = require('express')
const SyncServerImagesWithS3 = require('../../routines/SyncServerImagesWithS3')
const router = express.Router()
const TimeConstants = require('../../TimeConstants')

//Cache all message ids, so as not to respond multiple times to the same message
let DoNotRespond = []
let ToSoonForUpdate = false
//Wait time to stall routine (for successive calls)
const WAIT_TIME = TimeConstants.minutes

const stallRoutine = async ()=>{
    console.log(`SyncServerImagesWithS3 set to run in ${WAIT_TIME/1000} seconds as a result of an S3 change`)
    ToSoonForUpdate=true
    setTimeout(runRoutine,WAIT_TIME)
}

const runRoutine = async ()=>{
    console.log(`SyncServerImagesWithS3 running as a result of S3 change`)
    ToSoonForUpdate = false
    let SyncSuccess = await SyncServerImagesWithS3()
    console.log(SyncSuccess)
}

router.post('*', async (req,res,next)=>{

    let headers = req.headers

    //If this was not a verified request or the headers don't match then pass 
    if(!req.SignatureVerfiedAwsRequest || headers['x-amz-sns-message-type'] !== 'Notification') {
        return next()
    }
    let body = JSON.parse(req.body)

    //Get msg id
    let MessageID = headers['x-amz-sns-message-id']

    //If this request has already been processed
    let shouldRespond = !DoNotRespond.includes(MessageID)

    if(!shouldRespond) {
        if(DEBUG_ROUTES) {
            console.log(`MessageID ${MessageID} matches the MessageID of a previous request`)
            console.log(`Sending 200: OK and ending process`)
        }
        return res.sendStatus(200)
    }

    //This is a unique request

    //Cache the request id
    DoNotRespond.push(headers['x-amz-sns-message-id'])

    //Send ok
    res.sendStatus(200)

    let Message= JSON.parse(body.Message)
    let Records= Message.Records[0]
    let SourceIP= Records.requestParameters.sourceIPAddress
    let EventTime= Records.eventTime
    let EventName= Records.eventName
    let BucketName = Records.s3.bucket.name
    let ImageAffected= Records.s3.object.key

    if(DEBUG_ROUTES) {
        console.log(`--LOGGING S3 EVENT--`)
        console.log(`Source IP: ${SourceIP}`)
        console.log(`Event Time ${EventTime}`)
        console.log(`S3 Event: ${EventName}`)
        console.log(`Bucket: ${BucketName}`)
        console.log(`ImageAffected: ${ImageAffected}`)
    }

    //Check if update is emminent
    if(ToSoonForUpdate){
        console.log(`There were S3 changes, but SyncServerImagesWithS3 is already set to run`)
        //Too soon, update will happen shortly regaurdless of request
        return
    }

    //Running the routine in WAIT_TIME
    stallRoutine()
    
})

module.exports=router