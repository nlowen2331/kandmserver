const express = require('express');
const VerifyToken = require('../routes/VerifyToken');
const AcceptReservationRoutine = require('../routines/AcceptReservationRoutine');
const { StoredProcedures } = require('../writers/DBStoredProcs');
const { UseProcedure } = require('../writers/DBWorker');
const router = express.Router()

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to accept inquiry (V2)')

    //Make sure inquiry is acceptable (status<2 )
    let PrevInqStatus
    let error='none'
    try{
        PrevInqStatus = (await UseProcedure(StoredProcedures.select_inq_by_id,req.body.id))[0].status
    }
    catch(err){
        console.log(error)
        return res.status(500).send({Error: 'Database error'})
    }

    if(PrevInqStatus>=2){
        console.log(`Cannot accept this inquiry traditionally because its status is too high (<=2). Status: ${PrevInqStatus}`)
        //send improper request
        return res.status(400).send({Error: 'Cannot accept inquiry whose status is >=2'})
    }


    let InqRes
    try{
        InqRes = await UseProcedure(StoredProcedures.accept_inquiry,[req.body.id,req.payload.id,JSON.stringify(req.body.specs)])
    }
    catch(err) {
        error=err
    }
    if(error!='none') {
        console.log(error)
        return res.status(500).send({Error: 'Database error'})
    }

    //Send OK back and continue with code
    console.log('Successfully updated inquiry in the DB')
    res.sendStatus(200)

    if(req.body.specs.shouldSendAcceptance||req.body.specs.shouldBookDates) {

        let RoutineRes
        error='none'

        try{
            RoutineRes = await AcceptReservationRoutine({...req.body.specs,id: req.body.id})
        }
        catch(err) {
            error=err
        }

        if(error!=='none'){
            console.log(`AcceptReservationRoutine failed`)
            console.log(error)
            return
        }

        if(RoutineRes.OK){
            console.log('Successfully sent acceptance email to client')
        }

        else{
            console.log('There was an error sending acceptance email to client')
            console.log(RoutineRes.error)
        }

    }

});


module.exports = router