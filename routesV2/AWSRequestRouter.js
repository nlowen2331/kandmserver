const express = require('express')
const AWSRequestRouter = express.Router()
const ValidateRequestRoute = require('./AWSRequests/ValidateRequestRoute')
const SubscriptionConfirmationRoute = require('./AWSRequests/SubscriptionConfirmationRoute')
const BucketUpdateRoute = require('./AWSRequests/BucketUpdateRoute')
const NoSuitableRoute = require('./AWSRequests/NoSuitableRoute')

AWSRequestRouter.use('*', ValidateRequestRoute)
AWSRequestRouter.use('*', SubscriptionConfirmationRoute)
AWSRequestRouter.use('*', BucketUpdateRoute)

//If signature was verified but there was no suitable route

AWSRequestRouter.use('*', NoSuitableRoute)

module.exports=AWSRequestRouter