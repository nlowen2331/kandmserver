const express = require('express')
const router = express.Router()
const { UseProcedure } = require('../writers/DBWorker.js')
const { StoredProcedures } = require('../writers/DBStoredProcs.js')

router.get('/:unitid', async (req, res, next) => {

    console.log('Server asked to select price manipulators for specified unit')

    //recieve dbres
    let DBRes
    let error='none'
    try{
        DBRes = await UseProcedure(StoredProcedures.select_pricem_by_unitid,req.params.unitid,true)
        DBRes=DBRes[0].priceManipulators
    }
    catch(err){
        error=err
    }

    if(error!=='none'){
        console.log(`Error retrieving price manipulators from DB`)
        console.log(error)
        //On error send 500
        return res.status(500).send({msg: `DB Error`})
    }

    res.status(200).send({res: DBRes})
    
    //

});


module.exports = router