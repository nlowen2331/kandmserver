const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')

/*
    Takes in post request with 
    [{ unitid, picture}[,]]

    1. Authorization
    2. Get 'pictures' for {unitid} from the db 
    3. Search JSON for picture.picture_key
    4. Update that db picture with the post request body
    5. Return/Respond with err/success
*/

let lock = []

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to update picture information')

    if (req.headers["content-type"] !== 'application/json') {
        //Unsupported Media Type sent if not JSON
        return res.sendStatus(415)
    }

    //Gets unitid and corresponding pictures from DB
    let DBRes = await UseProcedure(StoredProcedures.verify_unit_owner, [req.body.unitid, req.payload.id], true)


    //Could not verify unit ownership
    if (DBRes.length < 1) {
        return res.sendStatus(403)
    }


    let Unit = DBRes[0].unitid
    let Pictures = JSON.parse(DBRes[0].pictures)
    let ChangesArray = req.body.changes

    //To avoid data loss, do not allow this process to run twice for the same unitid
    if (lock.includes(Unit)) {
        //Service unavailable
        return res.sendStatus(503)
    }

    //Set lock on this unitid
    lock.push(Unit)

    //for each change manipulate the picture data
    let Errors=[]

    ChangesArray.forEach(change => {
        let index = Pictures.findIndex(picture => picture.picture_id === change.key)
        switch (change.evName) {
            case `changeIsHidden`:
                Pictures[index] = { ...Pictures[index], isHidden: change.value }
                break;
            case `changeIsCover`:
                //Clear cover picture
                Pictures.forEach(picture=>{
                    picture.isCover=false
                })
                Pictures[index] = { ...Pictures[index], isCover: change.value }
                break;
            //Bad evName
            default:
                Errors.push({err: `Bad Event Name ${change.evName}`})
                break;
        }
    })

    //Do not update the DB upon errors, instead send back an improper request
    //along with the Error array
    if(Errors.length>0) {
        UnLock(Unit)
       return res.status(409).send({Errors: Errors})
    }

    //Check for conflicts in the data that will be used, don't update if they exist

    //Only 1 cover
    let coverFound = false
    let uniqueCoverError = !Pictures.every(picture=>{
        if(picture.isCover) {
            if(coverFound) {
                return false
            }
            coverFound=true
        }
        return true
    })

    //Hit them with an improper request
    if(uniqueCoverError||!coverFound) {
        UnLock(Unit)
        return res.status(409).send({Errors: [`There must be exactly 1 cover`]})
    }

    //Cover cannot be hidden

    let cover = Pictures.find(picture => picture.isCover === true)

    if(cover.isHidden){
        UnLock(Unit)
        return res.status(409).send({Errors: [`The cover must not be hidden`]})
    }

    //Accept the new array and update the DB with the new information
    let InsertResponse
    let DB_Error='none'
    try{
        InsertResponse = await UseProcedure(StoredProcedures.insert_pictures,[Unit,JSON.stringify(Pictures)])
    }
    catch(err) {
        DB_Error=err
    }

    if(DB_Error!=='none'){
        return res.status(500).send({Errors: ['DB Error']})
    }

    console.log(`Successfully inserted new picture data`)

    if(InsertResponse.affectedRows<1) {
        UnLock(Unit)
        return res.status(200).send({msg: 'No rows affected'})
    }
    else{
        UnLock(Unit)
        return res.status(200).send({msg: `Data was affected`})
    }

})

const UnLock=(unitid)=>{
    let lockIndex = lock.findIndex(lock=>lock===unitid)
    lock = lock.slice(lockIndex,lockIndex)
}

module.exports = router
