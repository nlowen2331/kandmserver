const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')

/*
    Takes in post request with 
    [{ unitid, specChange}[,]]

    1. Authorization
    2. Get 'specifications' for {unitid} from the db 
    3. iterate through specchanges
    4. replace matching
    5. Return/Respond with err/success
*/

let lock = []

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to update specifications for a unit')

    if (req.headers["content-type"] !== 'application/json') {
        //Unsupported Media Type sent if not JSON
        return res.sendStatus(415)
    }

    //Verify unit owner
    let DBRes = await UseProcedure(StoredProcedures.verify_unit_owner2, [req.body.unitid, req.payload.id], true)


    //Could not verify unit ownership
    if (DBRes.length < 1) {
        return res.sendStatus(403)
    }


    let Unit = DBRes[0].unitid
    
    //Get specifications for given unit
    let Specs_Res = await UseProcedure(StoredProcedures.select_specs_for_unit,Unit,true)

    //If no data, send server error
    if(Specs_Res.length<1) {
        return res.sendStatus(500)
    }

    //To avoid data loss, do not allow this process to run twice for the same unitid
    if (lock.includes(Unit)) {
        //Service unavailable
        return res.sendStatus(503)
    }

    //Set lock on this unitid
    lock.push(Unit)

    //Get the json
    let Specs
    try{
        Specs=JSON.parse(Specs_Res[0].specifications)
    }
    catch(err) {
        return res.status(500).send({Errors: [`Could not parse DB Records`]})
    }
    
    //Get Changes
    let ChangesArray=req.body.changes

    //for each change manipulate the picture data
    let Errors=[]

    ChangesArray.forEach(change => {
        
        switch (change.evName) {
            case 'changeGuests':
                Specs.guests = change.value
                break
            case 'changeSleeps':
                Specs.ammenities.sleeps = change.value
                break
            case 'changeBathrooms':
                Specs.ammenities.bathrooms = change.value
                break
            case 'changeBedrooms':
                Specs.ammenities.bedrooms = change.value
                break
            case 'changeParking':
                Specs.ammenities.parking = change.value
                break
            case 'changePetFee':
                Specs.pet_fee = change.value
                break
            case 'changeCleaningFee':
                Specs.cleaning_fee = change.value
                break
            case 'changeAllowPets':
                Specs.rules.pets = change.value
                break
            case 'changeAllowSmoking':
                Specs.rules.smoking = change.value
                break
            //Bad evName
            default:
                Errors.push({err: `Bad Event Name ${change.evName}`})
                break;
        }
    })

    //Do not update the DB upon errors, instead send back an improper request
    //along with the Error array
    if(Errors.length>0) {
        UnLock(Unit)
       return res.status(409).send({Errors: Errors})
    }

    //Make sure neccessary fields exist, if not send improper request
    if(Specs.guests==null || Specs.city==null || Specs.state==null, Specs.pet_fee==null,Specs.cleaning_fee==null
        || Specs.ammenities==null || Specs.rules==null) {
            return res.status(409).send({Errors: [`Important fields were missing`]})
        }
    console.log(Specs)
    
    //Accept the new array and update the DB with the new information
    let InsertResponse
    let DB_Error='none'
    try{
        InsertResponse = await UseProcedure(StoredProcedures.update_specs_for_unit,[Unit,JSON.stringify(Specs),req.payload.id])
    }
    catch(err) {
        DB_Error=err
    }

    if(DB_Error!=='none'){
        console.log(DB_Error)
        return res.status(500).send({Errors: ['DB Error']})
    }

    console.log(`Successfully inserted specs`)

    if(InsertResponse.affectedRows<1) {
        UnLock(Unit)
        return res.status(200).send({msg: 'No rows affected'})
    }
    else{
        UnLock(Unit)
        return res.status(200).send({msg: `Data was affected`})
    }

})

const UnLock=(unitid)=>{
    let lockIndex = lock.findIndex(lock=>lock===unitid)
    lock = lock.slice(lockIndex,lockIndex)
}

module.exports = router
