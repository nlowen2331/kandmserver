const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')

router.get('*', (req,res,next)=>{
	console.log('Server was pinged')
	res.send({
		message: "Pong!",
		error: dictionary.NO_ERROR
	});
});

router.post('*',(req,res,next)=>{
	console.log('Server successfully pinged by Amazon')
	console.log(req.body)
})


module.exports = router