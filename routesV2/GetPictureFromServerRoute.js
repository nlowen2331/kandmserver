const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
const { exp, Validator } = require('../ParamExpressions.js')
const fs = require('fs').promises

router.get('/:unitid/:imagename', async (req,res,next) =>{

    let folder = req.params.unitid
    let file = req.params.imagename

    let path = `./images/${folder}/${file}`

    console.log(`Sever asked to retrieve image ${path}`)

    if (!Validator(folder, exp.unitid) || !Validator(file,exp.s3Key)) {
        console.log("Improper request")
        res.send({
            message: "Improper request",
            error_code: dictionary.IMPROPER_REQUEST
        });

        return
    }

    let error='none'

    let File = await fs.readFile(path).catch(err=>{
        error=err
    })

    if(error!=='none'){
        console.log(err)
        res.send({
            message: "Error accessing server resource",
            error_code: dictionary.IO_ERROR
        });
        return
    }

    let fileSize = File.byteLength

    res.set({
        'Content-Type':'image/jpeg',
        'Content-Length': fileSize
    })

    console.log(`Sent image response for ${path}`)
    res.send(File)

})

module.exports=router