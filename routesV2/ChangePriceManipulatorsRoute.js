const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')
const dayjs = require('dayjs')
const currency = require('currency.js')

/*
    Takes in post request with 
    [{ unitid, calenderChangeStack}]

    Copied code template
*/

let lock = []

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to update price manipulators for a given unit')

    if (req.headers["content-type"] !== 'application/json') {
        //Unsupported Media Type sent if not JSON
        return res.sendStatus(415)
    }

    //Verify unit owner
    let DBRes = await UseProcedure(StoredProcedures.verify_unit_owner2, [req.body.unitid, req.payload.id], true)


    //Could not verify unit ownership
    if (DBRes.length < 1) {
        return res.sendStatus(403)
    }

    let Unit = DBRes[0].unitid
    
    //Get specifications for given unit
    let PriceM_Res = await UseProcedure(StoredProcedures.select_pricem_by_unitid,Unit,true)

    //If no data, send server error
    if(PriceM_Res.length<1) {
        return res.sendStatus(500)
    }

    //To avoid data loss, do not allow this process to run twice for the same unitid
    if (lock.includes(Unit)) {
        //Service unavailable
        return res.sendStatus(503)
    }

    //Set lock on this unitid
    lock.push(Unit)

    //Get the json
    let PriceM
    try{
        PriceM=JSON.parse(PriceM_Res[0].priceManipulators)
    }
    catch(err) {
        return res.status(500).send({Errors: [`Could not parse DB Records`]})
    }

    //If there is not price M data init
    if(PriceM==null) {
        PriceM=[]

        //Init in DB
        let InsertInitResponse
        let DB_Init_Error='none'
        try{
            InsertResponse = await UseProcedure(StoredProcedures.update_pricem_for_unit,[Unit,JSON.stringify(PriceM),req.payload.id])
        }
        catch(err) {
            DB_Error=err
        }
        //catch init errors
        if(DB_Error!=='none'){
            return res.status(500).send({Errors: ['Error on attempt to initialize a priceManipulator array']})
        }
        else{
            console.log(`Initialized new price manipulator data in DB for ${Unit}`)
        }
    }

    //Get Changes
    let ChangesArray=req.body.changes

    //for each change manipulate the calender data
    let Errors=[]

    //For each change
    ChangesArray.forEach(change => {
        let Target = PriceM.find(manipulator=>manipulator.id===change.key)
        if(Target==null&&change.evName!=='addManipulator'){
            Errors.push(`Could not find price manipulator for id: ${change.key}`)
            return;
        }
        switch(change.evName) {
            case 'addDates':
                Target.dates=[...Target.dates,...change.dates]
                break;
            case 'removeDates':
                let updatedDates = []
                //Filter out all dates that have a counterpart in change.dates
                Target.dates.forEach(target_date=>{
                    if(change.dates.findIndex(date_to_remove=>dayjs(date_to_remove).isSame(dayjs(target_date),'day'))===-1){
                        updatedDates.push(target_date)
                    }
                })
                Target.dates=updatedDates
                break;
            case 'changeManipulatorValues':
                if(change.toChange==null||change.toChange.length<1){
                    Errors.push(`Empty toChange array for manipulator with id: ${change.key} `)
                    break;
                }
                //Iterate through the values that need changing
                change.toChange.forEach(value_change=>{
                    switch(value_change){
                        case 'enabled':
                            if(change.value.enabled){
                                Target.enabled=true
                            }
                            else{
                                Target.enabled=false
                            }
                            break;
                        case 'value':
                            switch(change.value.operation){
                                case '+':
                                case '-':
                                case '*':
                                case '/':
                                    Target.oepration=change.value.operation
                                    break;
                                default:
                                    Errors.push(`Improper value supplied as operation ${change.value.operation}`)
                            }
                            if(typeof change.value.value === 'number' && /*Test for NaN*/change.value.value===change.value.value){
                                Target.value=change.value.value
                            } 
                            else{
                                Errors.push(`Improper value supplied as value ${change.value.value}`)
                            }  
                            break;
                        default:
                            Errors.push(`The given 'changeManipulator' value was invalid ${value_change}`)
                            break;
                    }
                })
                break;
            case 'renameManipulator':
                if(PriceM.findIndex(manipulator=>manipulator.name===change.value)!==-1){
                    Errors.push(`Cannot accept manipulator name: ${change.value} because it already exists`)
                    break;
                }
                Target.name=change.value
                break;
            case 'removeManipulator':
                PriceM=PriceM.filter(manipulator=>manipulator.id!==change.key)
                break;
            case 'addManipulator':
                //prevValue is used to store the genorated manipulator
                if(PriceM.findIndex(manipulator=>manipulator.name===change.prevValue.name)!==-1) {
                    Errors.push(`Cannot accept manipulator name: ${change.prevValue.name} because it already exists`)
                    break;
                }
                if(PriceM.findIndex(manipulator=>manipulator.id===change.prevValue.id)!==-1) {
                    Errors.push(`Cannot accept manipulator id: ${change.prevValue.id} because it already exists`)
                    break;
                }
                PriceM=[...PriceM,change.prevValue]
                break;
            default: 
                Errors.push(`Unknown evName given ${change.evName}`)
                break;
        }
    })

    //Do not update the DB upon errors, instead send back an improper request
    //along with the Error array
    if(Errors.length>0) {
        UnLock(Unit)
       return res.status(409).send({Errors: Errors})
    }

    //Accept the new array and update the DB with the new information
    let InsertResponse
    let DB_Error='none'
    try{
        InsertResponse = await UseProcedure(StoredProcedures.update_pricem_for_unit,[Unit,JSON.stringify(PriceM),req.payload.id])
    }
    catch(err) {
        DB_Error=err
    }

    if(DB_Error!=='none'){
        return res.status(500).send({Errors: ['DB Error']})
    }

    console.log(`Successfully inserted price manipulator changes for unit: ${Unit}`)

    if(InsertResponse.affectedRows<1) {
        UnLock(Unit)
        return res.status(200).send({msg: 'No rows affected'})
    }
    else{
        UnLock(Unit)
        return res.status(200).send({msg: `Data was affected`})
    }

})

const UnLock=(unitid)=>{
    let lockIndex = lock.findIndex(lock=>lock===unitid)
    lock = lock.slice(lockIndex,lockIndex)
}

module.exports = router
