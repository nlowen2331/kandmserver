const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')

//Takes in {changes: [{evName,value}[,]], unitid: }
let lock = []

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to update some misc changes for Unit')

    if (req.headers["content-type"] !== 'application/json') {
        //Unsupported Media Type sent if not JSON
        return res.sendStatus(415)
    }

    //Gets unitid and corresponding pictures from DB
    let DBRes = await UseProcedure(StoredProcedures.verify_unit_owner2, [req.body.unitid, req.payload.id], true)


    //Could not verify unit ownership
    if (DBRes.length < 1) {
        return res.sendStatus(403)
    }


    let Unit = DBRes[0].unitid
    let ChangesArray = req.body.changes

    //To avoid data loss, do not allow this process to run twice for the same unitid
    if (lock.includes(Unit)) {
        //Service unavailable
        return res.sendStatus(503)
    }

    //Set lock on this unitid
    lock.push(Unit)

    //for each change manipulate the picture data
    let Errors=[]

    for(let i=0;i<ChangesArray.length;i++){
        let Change = ChangesArray[i]
        let DB_Res
        switch(Change.evName){
            case 'changeUnitName':
                try{
                    DBRes = await UseProcedure(StoredProcedures.update_unit_name,[Unit,Change.value])
                }
                catch(err){

                    if(DEBUG_ROUTES){
                        console.log('Error storing misc owner data')
                        console.log(err)
                    }

                    Errors.push({err: `DB Failure for event: ${Change.evName},${Change.value}`})
                }
                break;
            case 'changeUnitDesc':
                try{
                    DBRes = await UseProcedure(StoredProcedures.update_unit_desc,[Unit,Change.value])
                }
                catch(err){

                    if(DEBUG_ROUTES){
                        console.log('Error storing misc owner data')
                        console.log(err)
                    }

                    Errors.push({err: `DB Failure for event: ${Change.evName},${Change.value}`})
                }
                break;
            default:
                Errors.push({err: `Bad Event Name ${change.evName}`})
                break;
        }
    }
    

    //Do not update the DB upon errors, instead send back an improper request
    //along with the Error array
    if(Errors.length>0) {
        UnLock(Unit)
       return res.status(409).send({Errors: Errors})
    }

    console.log(`Successfully updated DB with misc data`)
    UnLock(Unit)

    res.status(200).send({OK:true})

})

const UnLock=(unitid)=>{
    let lockIndex = lock.findIndex(lock=>lock===unitid)
    lock = lock.slice(lockIndex,lockIndex)
}

module.exports = router
