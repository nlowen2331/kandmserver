const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const { exp, Validator } = require('../ParamExpressions.js')
require('dotenv').config()

//This is bugged don't point any code to this

router.post('*', (req, res, next) => {

    console.log('Server asked to authenticate user')
    console.log(req.body)

    if (!Validator(req.body.password, exp.password) || !Validator(req.body.username, exp.username)) {
        console.log("The request contained an improper body")
        res.send({
            message: "The request contained an improper body",
            error_code: dictionary.IMPROPER_REQUEST
        });
        return
    }

    let proc_res = UseProcedure(StoredProcedures.select_user, req.body.username, true).then(owner_res => {
        if (owner_res.length < 1) {
            console.log("No user found with given information")
            res.send({
                message: "No user found",
                error_code: dictionary.NO_MATCHING_USER
            });
            return
        }

        console.log("Found user " + proc_res[0].username + ", matching password")

        bcrypt.compare(req.body.password, proc_res[0].password).then((isEncrypted) => {
            if (!isEncrypted) {
                console.log("User: " + proc_res[0].username + " submitted incorrect password")
                res.send({
                    message: "Incorrect password",
                    error_code: dictionary.INCORRECT_PASSWORD
                });
                return
            }

            const payload = {
                id: proc_res[0].ownerid,
                user: proc_res[0].username,
                privilege: proc_res[0].privilege
            }

            jwt.sign({ payload }, process.env.SIGNING_KEY, { expiresIn: '1d' }, (err, token) => {
                if (err) {
                    console.log('Error signing the JWT')
                    console.log(err)
                    res.send({
                        message: 'Error signing the JWT',
                        error_code: dictionary.JWT_SIGNING_ERROR
                    });
                    return
                }

                console.log("Sending token to " + proc_res[0].username + "\nwith payload: " + JSON.stringify(payload))
                res.send({
                    token: token,
                    message: "Successfully logged in",
                    error_code: dictionary.NO_ERROR
                });

            })

        })

    }).catch(err=>{
        console.log("There was an error calling the proc")
        console.log(err)
        res.send({
            message: "Some procedure error",
            error_code: dictionary.PROCEDURE_ERROR
        });
    })

})



module.exports = router