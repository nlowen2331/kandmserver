const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken.js');
const { GetBucketObject, UploadFileToS3 } = require('../fetchers/S3Fetcher.js');
const multer  = require('multer');
const upload = multer({ dest: 'uploads/' })
const fs = require('fs').promises
require('dotenv').config()

router.post('*', upload.single(`lease`), VerifyToken,async (req, res, next) => {

    console.log('Server asked to upload new lease to S3')
    console.log(req.file)

    let Lease = await fs.readFile(req.file.path)

    let Response
    let error = 'none'
    try{
        Response = await UploadFileToS3({Bucket: req.body.bucket,Key: req.body.key, Body: Lease})
    }
    catch(err){
        error=err
    }

    if(error!=='none'){
        if(DEBUG_ROUTES) {
            console.log('Error uploading lease to S3')
            console.log(error)
        }
        return res.status(500).send({Error: [error]})
    }

    console.log('Successfully posted lease to S3')

    return res.sendStatus(200)
    
});


module.exports = router