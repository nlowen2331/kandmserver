const express = require('express');
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken');
const { StoredProcedures } = require('../writers/DBStoredProcs');
const { UseProcedure } = require('../writers/DBWorker');

/*
    Accept an inquiry status change
    1. Verify that the owner can alter the status
    2. Get the existing status from the DB
    3. If the posted status is lower, reject
    4. Append the DB with the new inq status
    5. If the status is 2 (accepted) take extra actions

    The DB will be made to run checks on the status of all inquiries with status 3 (rejceted),
    Inquiries with a status 3 whose date of finalization is greater than a month will be removed
    from the DB.

    If status is set to 2:
    1. An additional call will be made to some Confirmation Handler
    the confirmation handler will send an email out to the email in the specific inquiry's
    specs. The server will also block off the date on the calender in airbnb.

    This block off, if need be, can be repealed in Airbnb, but in the future there could be
    an option for this 

*/

router.post('*', VerifyToken, async (req, res, next) => {

    console.log('Server asked to update status for inquiry '+req.body.inqid)

    //Select the inquiry
    let DBResI = await UseProcedure(StoredProcedures.select_inq_by_id,req.body.inqid,true)

    let Inquiry = DBResI[0]

    //Make sure onwer has permission
    let DBResVerify = await UseProcedure(StoredProcedures.verify_unit_owner2,[Inquiry.unitid,req.payload.id],true)

    //Send forbidden
    if(DBResVerify.length<1) {
        return res.status(403).send({Errors: [`This inquiry does not belong to token bearer`]})
    }

    //Compare posted status with db status
    if(Inquiry.status>req.body.status) {
        return res.status(409).send({Errors: [`You cannot lower an inquiry status using this route`]})
    }

    //Allow inquiry post
    let affected = false
    let DBResInsert = UseProcedure(StoredProcedures.update_inq_status,[Inquiry.id,req.body.status,req.payload.id])

    if(DBResInsert.rowsAffected<1) {
        res.status(200).send({msg: `Successfully posted status, but no row changes occured`})
    }

    else{
        res.status(200).send({msg: `Successfully posted status!`})
        affected=true
    }

    //Trigger a routine to email and block off dates 
    if(req.body.status===2 && affected) {
        console.log('Routine will be triggered!')
    }

});

module.exports=router