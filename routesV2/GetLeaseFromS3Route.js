const express = require('express')
const router = express.Router()
const VerifyToken = require('../routes/VerifyToken.js');
const { GetBucketObject } = require('../fetchers/S3Fetcher.js');
require('dotenv').config()
const {exp,Validator} = require('../ParamExpressions')

router.get('/:unitid', async (req, res, next) => {

    console.log('Server asked to grab lease from S3')

    if(!Validator(req.params.unitid,exp.unitid)){
        return res.status(400).send({Error: 'Illegal characters'})
    }

    let Lease
    let error='none'

    try{
        Lease = await GetBucketObject(`${req.params.unitid}.pdf`, process.env.LEASE_BUCKET)
    }
    catch(err) {

        if(DEBUG_ROUTES) {
            console.log(`Could not fetch S3 Object`)
            console.log(err)
            if(process.env.LEASE_BUCKET==null){
                console.log(`You have not set LEASE_BUCKET as an environment variable`)
            }
        }

        error=err.code
    }

    if(error!=='none'){

        switch(error.msg) {
            case 'NoSuchKey':
                return res.status(404).send({Error: `Lease does not exist in S3`})
            default:
                return res.status(500).send({Error: `Could not retrieve lease from AWS`})
        }
    }

    console.log('Successfully sent Lease file')
    
    res.set({
        'Content-Type':'application/pdf',
        'Content-Length': Lease.ContentLength
    })

    return res.status(200).send(Lease.Body)

});


module.exports = router