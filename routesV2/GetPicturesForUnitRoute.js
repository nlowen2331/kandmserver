const express = require('express')
const router = express.Router()
const dictionary = require('../ErrorCodeDictionary.js')
const { StoredProcedures } = require('../writers/DBStoredProcs')
const { UseProcedure } = require('../writers/DBWorker')
const { exp, Validator } = require('../ParamExpressions.js')

router.get('/:unitid', async (req, res, next) => {

    let unitid = req.params.unitid

    console.log(`Server asked for the picture listings of unit ${unitid}`)

    if (!Validator(unitid, exp.unitid)) {
        console.log("Improper unitid")
        res.send({
            message: "Improper unit id",
            error_code: dictionary.IMPROPER_REQUEST
        });

        return
    }

    let Pictures = await UseProcedure(StoredProcedures.select_unit_pictures, unitid, true)

    try {

        //Choose 1st response from array
        Pictures = Pictures[0]

        //Isolate the picture array from the response
        Pictures = Pictures.pictures

        //Parse it 
        Pictures = JSON.parse(Pictures)

        if (Pictures.length < 1) {
            console.log(`There was no picture data for unit ${unitid}`)
            res.send({
                message: "No records for that unit, most likely an error",
                error_code: dictionary.NO_MATCHING_DATA
            });
            return
        }

    }
    catch (err) {
        console.log(`Error determining data to send`)
        console.log(err)
        res.send({
            message: "There was an error processing this request",
            error_code: dictionary.UNKNOWN_ERROR
        });
    }


    //no errors
    res.send({
        pictures: Pictures,
        message: null,
        error_code: dictionary.NO_ERROR
    })

})

module.exports = router