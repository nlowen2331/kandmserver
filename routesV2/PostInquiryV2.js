const express = require('express');
const ConfirmInquiry = require('../routines/ConfirmInquiry');
const { StoredProcedures } = require('../writers/DBStoredProcs');
const { UseProcedure } = require('../writers/DBWorker');
const router = express.Router()

router.post('*', async (req, res, next) => {

    console.log('Server asked to post inquiry (V2)')

    //Just make sure the email is there for issues
    if(req.body.specs.email==null) {
        console.log('No email specified, could not accept inquiry')
        //Bad request
        return res.status(406).send({Error: 'Please give email address in request body'})
    }

    let InqRes
    let error='none'
    try{
        InqRes = await UseProcedure(StoredProcedures.insert_inquiryV2,[JSON.stringify(req.body.specs),JSON.stringify(req.body.priceInfo),req.body.id])
    }
    catch(err) {
        error=err
    }
    if(error!='none') {
        console.log(error)
        return res.status(500).send({Error: 'Database error'})
    }

    //Send OK back and continue with code
    console.log('Successfully posted inquiry to DB')
    res.sendStatus(200)

    //Invoke a routine that alerts the client email and host email about the inquiry
    let Response = await ConfirmInquiry({...req.body.specs,...req.body.priceInfo,unitid: req.body.id||req.body.specs.id})

    if(Response.OK) {
        console.log('Successfully sent an email to the client confirming inquiry')
    }
    else{
        console.log(Response)
    }

});


module.exports = router