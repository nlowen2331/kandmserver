const express = require('express')
const router = express.Router()

router.get('*', async (req, res, next) => {
    if (DEBUG_ROUTES) {
        console.log('Incoming GET Request')
        console.log(req.headers)
    }
    next()
});

router.post('*', async (req, res, next) => {

    if (DEBUG_ROUTES) {
        console.log('Incoming POST Request')
        console.log('headers:')
        console.log(req.headers)
        console.log('body:')
        console.log(req.body)
    }
    next()
});

module.exports = router