const dayjs = require('dayjs')

const fs = require('fs').promises

const PATH = `./reports`

//Expects an array of JSON objects
const LogProcedure = async (data, filename) => {
    let createDir = await CreateReportsDir()
    let now = dayjs()
    let pathDate = `${now.format(`YYYY.MM.DD_T_HH.mm.ss`)}`
    await fs.writeFile(`${PATH}/${filename}_${pathDate}.txt`, JSON.stringify(data))

    return ('Successfully wrote data to file')
}

const CreateReportsDir = async () =>{
    try {
        await fs.access(`${PATH}`)
    }
    catch {
        try {
            await fs.mkdir(`${PATH}`)
            console.log(`(Error Reporter): Created 'reports' directory`)
        }
        catch{
            throw new Error('Could not create reports folder in directory')
        }
        return('Created Reports Directory')
    }
    return('Reports directory exists')
}

module.exports = LogProcedure