
const fs = require('fs').promises

const PATH = `./reports`

//Expects an array of JSON objects
const LogProcedure = async (data, filename) => {
    let now = new Date()
    let pathDate = `${now.getDate()}.${now.getMonth() + 1}.${now.getFullYear()}.${now.getHours()}.${now.getMinutes()}.${now.getSeconds()}.${now.getMilliseconds()}`
    await fs.writeFile(`${PATH}/${filename}_${pathDate}.txt`, JSON.stringify(data)).catch(err => {
        throw new Error(`Error writing data to ${PATH}/${filename}_${pathDate}.txt`)
    })

    return ('Successfully wrote data to file')
}

module.exports = LogProcedure