//This module is incomplete and should not be imported or called ever, nothing in this code has been tested

const { request } = require('request')

const config = {
    username: process.env.AIRBNB_EMAIL,
    password: process.env.AIRBNB_PASSWORD,
    user_id: process.env.AIRBNB_HOSTID,
    headers: {
        'cache-control': 'no-cache',
        'user-agent': 'Airbnb/17.50 iPad/11.2.1 Type/Tablet',
        'content-type': 'application/json',
        'accept': 'application/json',
        'accept-encoding': 'br, gzip, deflate',
        'accept-language': 'en-us',
        'x-airbnb-oauth-token': process.env.AIRBNB_TOKEN,
        'x-airbnb-api-key': '915pw2pnf4h1aiguhph5gc5b2',
        'x-airbnb-locale': 'en',
        'x-airbnb-currency': 'USD',
    }
}
/*WARNING: Do not make excess calls to this route
    This function should only be called when our API token
    has expired
*/
const bnb_Authorize = async () => {
    let options = {
        method: 'POST',
        url: 'https://api.airbnb.com/v1/authorize',
        headers: JSON.parse(JSON.stringify(...config.headers)),
        gzip: true,
        form: {
            grant_type: 'password',
            password: config.password,
            username: config.username,
        }
    }
    options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
    request(options,(err,__,body)=>{
        if(err) {
            throw err
        }
        return body
    })
}

