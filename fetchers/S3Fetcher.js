const AWS = require('aws-sdk')
require('dotenv').config()

//Create S3 Agent

let WorkerRunning = false
let Agent 

const CreateS3Agent = async () => {
    if (process.env.SERVER_AWS_USER_ACCESS_KEY == null || process.env.SERVER_AWS_USER_SECRET_KEY == null
        || process.env.S3_PICTURES_REGION == null) {
        throw new Error('Missing environment variables... S3 Agent could not be created')
    }
    AWS.config.setPromisesDependency()
    AWS.config.update({
        accessKeyId: process.env.SERVER_AWS_USER_ACCESS_KEY,
        secretAccessKey: process.env.SERVER_AWS_USER_SECRET_KEY,
        region: process.env.S3_PICTURES_REGION
    })

    Agent=new AWS.S3()
    WorkerRunning=true
    return ('S3 Agent was initialized')
}

const GetAgent = ()=>{
    return Agent
}

//Get Object from S3 Bucket

const GetBucketObject = async (key, bucket) => {

    if (!WorkerRunning) {
        throw new Error('AWS S3 Agent has not been created')
    }

    const Object = await Agent.getObject({
        Bucket: bucket,
        Key: key
    }).promise()

    return Object
}

//Get Object Listing of an S3 Bucket

const GetBucketListing = async (bucket) => {

    if (!WorkerRunning) {
        throw new Error('AWS S3 Agent has not been created')
    }

    const List = await Agent.listObjectsV2({
        Bucket: bucket
    }).promise()

    return List
}

const GetAllBuckets = async ()=>{

    if (!WorkerRunning) {
        throw new Error('AWS S3 Agent has not been created')
    }

    const List = await Agent.listBuckets().promise()

    return List
}

//params --> {bucket, key, stream}
const UploadFileToS3 = async (params) =>{

    if (!WorkerRunning) {
        throw new Error('AWS S3 Agent has not been created')
    } 

    return await Agent.upload(params).promise()

}

module.exports = { GetBucketObject, GetBucketListing, CreateS3Agent,UploadFileToS3,GetAgent}