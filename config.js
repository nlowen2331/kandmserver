
/*
    These functions/constants represent the actual queries being performed
    If syntax changes these must update
*/
const getUnits = 'call select_units()'
const getMessages = 'call select_messages()'
const get_owner_ids = 'call select_all_ownerids()'
const getAirbnbIds = 'call select_unit_airbnbid()'
const getFolderless = 'call kandm.select_units_without_folders()'
const getUnitOwners = 'call kandm.select_unit_owners()'
const getAllUnitPictureInfo = 'call kandm.get_picture_info_all_units()t'
const postInquiry= (json,id)=> (`call kandm.insert_inquiry('${json}',"${id}")`)
const postMessage= (fname,lname,email,phone,message,json)=> (`call kandm.insert_msg('${fname}','${lname}','${email}','${phone}','${message}','${json}')`)
const insertCalenders = (unitid,calender)=>(`call kandm.insert_calenders('${unitid}','${calender}')`)
const getOwner= (username)=> (`call kandm.select_user('${username}')`)
const insert_owner= (username,password)=>(`call kandm.insert_owner('${username}','${password}')`)
const insert_minmax= (unitid,min,max)=>(`call kandm.insert_min_max_prices('${unitid}','${min}','${max}')`)
const getInquiriesByOwner= (owner)=>(`call kandm.select_inquiries_of_owner('${owner}')`)
const getInquiriesByStatus= (owner,status)=>(`call kandm.select_inquiries_with_status('${owner}',${status})`)
const getEmailBody= (unitid,type)=>(`call kandm.select_email_body('${unitid}','${type}')`)
const insert_error= (msg,email,unitid)=>(`call kandm.insert_errormsg('${msg}','${email}','${unitid}')`)
const update_status = (inqid,status,ownerid)=>(`call kandm.update_inq_status(${inqid},${status},${ownerid})`)
const update_status_msg = (msgid,statuses)=>(`call kandm.update_msg_status(${msgid},'${statuses}')`)
const update_specs = (inqid,json,ownerid)=>(`call kandm.update_inq_specs(${inqid},'${json}',${ownerid})`)
const post_comment = (inqid,comment,ownerid)=>(`call kandm.post_inq_comment(${inqid},'${comment}',${ownerid})`)
const get_calender_by_unit = (unitid) =>(`call kandm.select_calender_by_unitid('${unitid}')`)
const select_msg_statuses = (msgid)=> (`call kandm.select_msg_statuses_by_id(${msgid})`)
const select_owner = (ownerid)=>(`call kandm.select_owner(${ownerid})`)
const check_for_pictureless = (ownerid)=>(`call kandm.check_for_pictureless(${ownerid})`)
const update_unit_pictures = (unitid,JSON)=>(`call kandm.update_unit_pictures('${unitid}','${JSON}')`)
const verify_unit_owner = (unitid,ownerid)=>(`call kandm.verify_unit_owner('${unitid}',${ownerid})`)

exports.getUnits=getUnits
exports.postInquiry=postInquiry
exports.postMessage=postMessage
exports.getAirbnbIds=getAirbnbIds
exports.insertCalenders=insertCalenders
exports.getOwner=getOwner
exports.insert_owner=insert_owner
exports.insert_minmax=insert_minmax
exports.getInquiriesByOwner=getInquiriesByOwner
exports.getEmailBody=getEmailBody
exports.insert_error=insert_error
exports.update_status=update_status
exports.update_specs=update_specs
exports.post_comment=post_comment
exports.get_calender_by_unit=get_calender_by_unit
exports.get_owner_ids=get_owner_ids
exports.getMessages = getMessages
exports.update_status_msg=update_status_msg
exports.select_msg_statuses=select_msg_statuses
exports.select_owner=select_owner
exports.getInquiriesByStatus=getInquiriesByStatus
exports.getFolderless=getFolderless
exports.getUnitOwners=getUnitOwners
exports.check_for_pictureless=check_for_pictureless
exports.update_unit_pictures=update_unit_pictures
exports.verify_unit_owner=verify_unit_owner
exports.getAllUnitPictureInfo=getAllUnitPictureInfo
