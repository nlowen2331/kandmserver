const TimeConstants = {
    milliseconds: 1,
    seconds: 1000,
    minutes: 60*1000,
    hours: 60*60*1000,
}

module.exports = TimeConstants