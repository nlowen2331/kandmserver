
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')

//v2 routes
const PingRoute = require('../routesV2/PingRoute')
const AuthenticationRoute = require('../routes/AuthenticationRoute')
const GetCalenderByUnitRoute = require('../routesV2/GetCalenderByUnitRoute')
const GetPicturesForUnitRoute = require('../routesV2/GetPicturesForUnitRoute')
const GetPictureFromServerRoute = require('../routesV2/GetPictureFromServerRoute')
const ChangePictureInfoRoute = require('../routesV2/ChangePictureInfoRoute')
const ChangeSpecsRoute = require('../routesV2/ChangeSpecsRoute')
const ChangeCalenderInfoRoute = require('../routesV2/ChangeCalenderInfoRoute')
const ChangePriceManipulatorsRoute = require('../routesV2/ChangePriceManipulatorsRoute')
const AlterInquiryStatusRoute = require('../routesV2/AlterInquiryStatusRoute')
const PostInquiryV2Route = require('../routesV2/PostInquiryV2')
const GetPriceManipulatorsRoute = require('../routesV2/GetPriceManipulatorsRoute')
const GetLeaseFromS3Route = require('../routesV2/GetLeaseFromS3Route')
const PostLeaseToS3Route = require('../routesV2/PostLeaseToS3Route')
const AcceptInquiryRouteV2 = require('../routesV2/AcceptInquiryRouteV2')
const PostMiscChangesRoute = require('../routesV2/PostMiscChangesRoute')
//v1 routes
const GetUnitsRoute = require('../routes/GetUnitsRoute')
const MessageRoute = require('../routes/PostMessageRoute')
const LoginRoute = require('../routes/LoginRoute')
const GetOwnerInquiriesRoute = require('../routes/GetOwnerInquiriesRoute')
const UpdateStatusRoute = require('../routes/UpdateInqStatusRoute')
const UpdateMessageStatusRoute = require('../routes/UpdateMsgStatusRoute')
const UpdateSpecsRoute = require('../routes/UpdateInqSpecsRoute')
const PostCommentRoute = require('../routes/PostCommentRoute')
const SelectMessagesRoute = require('../routes/GetMessages')
const SelectOwnerRoute = require('../routes/GetOwnerRoute')
const InitRequestMiddleware = require('../routesV2/InitRequestMiddleware')
const AWSRequestRouter = require('../routesV2/AWSRequestRouter')

//require GetOwnerIds for legacy route support
require('../GetOwnerIds')

//Set up server

let ExpressListener

const CreateExpressServer = async () => {

    ExpressListener = express()

    const allowCrossDomain = (req, res, next) => {
        res.header('Access-Control-Allow-Origin', "*")
        res.header('Access-Control-Allow-Methods', 'GET, POST,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization')
        next()
    }

    ExpressListener.use(allowCrossDomain)

    ExpressListener.use(express.json())

    const port = process.env.PORT || 4000

    ExpressListener.listen(port)
    
    //parse the body?
    ExpressListener.use('*',bodyParser.text({type: 'text/plain'}))

    //Handle all requests
    ExpressListener.use('*',InitRequestMiddleware)

    //Handle AWS Requests
    ExpressListener.use('*',AWSRequestRouter)

    //List of routes

    //v2 routes
    ExpressListener.use('/Ping', PingRoute)
    ExpressListener.use('/SelectCalenderByUnit', GetCalenderByUnitRoute)
    ExpressListener.use('/GetPictureListingForUnit',GetPicturesForUnitRoute)
    ExpressListener.use('/GetImage',GetPictureFromServerRoute)
    ExpressListener.use('/PostPictureChanges',ChangePictureInfoRoute)
    ExpressListener.use('/PostSpecsChanges',ChangeSpecsRoute)
    ExpressListener.use('/UpdateStatus',AlterInquiryStatusRoute)
    ExpressListener.use('/Inquiry', PostInquiryV2Route)
    ExpressListener.use('/PostCalenderChanges',ChangeCalenderInfoRoute)
    ExpressListener.use('/PostPriceMChanges',ChangePriceManipulatorsRoute)
    ExpressListener.use('/GetPriceManipulatorsForUnit',GetPriceManipulatorsRoute)
    ExpressListener.use('/GetLeaseForUnit',GetLeaseFromS3Route)
    ExpressListener.use('/PostLeaseToS3',PostLeaseToS3Route)
    ExpressListener.use('/AcceptInquiryV2',AcceptInquiryRouteV2)
    ExpressListener.use('/PostMiscChanges',PostMiscChangesRoute)

    //v1 routes
    ExpressListener.use('/GetUnits', GetUnitsRoute)
    ExpressListener.use('/Message', MessageRoute)
    ExpressListener.use('/Login', LoginRoute)
    ExpressListener.use('/GetInquiryByOwner', GetOwnerInquiriesRoute)
    //ExpressListener.use('/UpdateStatus', UpdateStatusRoute)
    ExpressListener.use('/UpdateSpecs', UpdateSpecsRoute)
    ExpressListener.use('/PostOwnerComment', PostCommentRoute)
    ExpressListener.use('/GetMessages', SelectMessagesRoute)
    ExpressListener.use('/UpdateMsgStatus', UpdateMessageStatusRoute)
    ExpressListener.use('/GetOwner', SelectOwnerRoute)
    ExpressListener.use('/Authenticate', AuthenticationRoute)

    ExpressListener.use(express.static(path.join(__dirname, '../app_build')));

    ExpressListener.get('/*', (req,res)=> {
        res.sendFile(path.join(__dirname, '../app_build', 'index.html'));
    });
   

    return (`Server is listening on port ${port}`)
}

module.exports = CreateExpressServer


