const bnbhelper = require('airbnbapijs')
require('dotenv').config()

const config = {
    token: 'ci34lne11nq46q4gkoinnyo40'
}

const TestFunction = async () =>{

   console.log(config.token)
   let AuthSuccess =  await bnbhelper.testAuth(config.token)
   console.log(AuthSuccess)

   let Res = await bnbhelper.setAvailabilityForDay({
       token: config.token,
       id: 20820066,
       date: '2021-04-13',
       availability: 'unavailable_persistent',
       note: `Booked automatically by sandandskirental.com`
   })

   console.log(Res)

   return ({OK: true})
}

TestFunction().then(res=>{
    console.log(res)
}).catch(err=>{
    console.log(err)
})