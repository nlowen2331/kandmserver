require('dotenv').config()

const bnbhostapi = {
  username: process.env.AIRBNB_EMAIL,
  password: process.env.AIRBNB_PASSWORD,
  user_id: process.env.AIRBNB_HOSTID,
  token: process.env.AIRBNB_TOKEN,
  headers: {
    'cache-control': 'no-cache',
    'user-agent': 'Airbnb/17.50 iPad/11.2.1 Type/Tablet',
    'content-type': 'application/json',
    'accept': 'application/json',
    'accept-encoding': 'br, gzip, deflate',
    'accept-language': 'en-us',
    'x-airbnb-oauth-token': process.env.AIRBNB_TOKEN,
    'x-airbnb-api-key': '915pw2pnf4h1aiguhph5gc5b2',
    'x-airbnb-locale': 'en',
    'x-airbnb-currency': 'USD',
  }
}

module.exports=bnbhostapi
