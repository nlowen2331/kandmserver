
var config = require('./config');
const dayjs = require("dayjs");
const bnbhelper = require('airbnbapijs')

const PostCalenderV2 = async ({listing_id,arrival,departure}) =>{

    if(DEBUG_POSTCALV2){
        console.log('PostCalenderV2 Called')
        console.log({
            listing_id: listing_id,
            arrival: arrival,
            departure, departure,
        })
    }

    let DateArray = []
    let TempDate = arrival

    while(!dayjs(TempDate).isSame(dayjs(departure),'day')){
        DateArray.push(dayjs(TempDate).format(`YYYY-MM-DD`))
        TempDate=dayjs(TempDate).add(1,'day')
    }

    let BookingResponses = await Promise.all(DateArray.map(date=>BookingRequest(listing_id,date)))

    if(DEBUG_POSTCALV2){
        console.log('Logging AirBnB Response for each date')
        console.log(BookingResponses)
    }

    return({OK: true})
    
}

const BookingRequest = async(listing_id,date /*YYYY-MM-DD*/)=>{

    if(DEBUG_POSTCALV2){
        console.log(`Booking date: ${date}`)
    }

    let Res = await bnbhelper.setAvailabilityForDay({
        token: config.token,
        id: listing_id,
        date: date,
        availability: 'unavailable_persistent'
    })

    return {...Res,OK: true}
}

module.exports = PostCalenderV2;
