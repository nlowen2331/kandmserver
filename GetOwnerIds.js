const { _GetOwnerIdArrayIsLoaded } = require("./routines/CacheOwnerIds")

console.log('Legacy GetOwnerIds.js is being used by some server component')
console.log('Hooking into global.OwnerIdArray...')

const getIdsLoaded = ()=>{return _GetOwnerIdArrayIsLoaded}
const getOwnerIds = ()=>{return global.OwnerIdArray}

module.exports = { getIdsLoaded,getOwnerIds }