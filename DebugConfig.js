

const DebugOptions = [
    {name : 'DEBUG_ALL', on: false},
    {name : 'DEBUG_MIN_MAX_PRICES', on: false},
    {name: 'DEBUG_PRICEM', on:false},
    {name : 'DEBUG_DB_WORKER', on: false},
    {name : 'DEBUG_DB_WORKER_RES', on: false},
    {name : 'DEBUG_ROUTES', on: true},
    {name: 'DEBUG_EMAILER', on: false},
    {name: 'DEBUG_SYNC', on: false},
    {name: 'DEBUG_TIME_HAWK', on: false},
    {name: 'DEBUG_HASH',on: false},
    {name: 'DEBUG_POSTCALV2', on: true}
]

//If DEBUG_ALL is on, then set every option to true
if(DebugOptions[0].on) {
    DebugOptions.forEach(option=>{
        option.on=true
    })
}

//Assign global vars true or false based on option
DebugOptions.forEach(option=>{
    global[option.name]=option.on
    if(option.on){
        console.log(`Debugging for ${option.name}`)
    }
})

