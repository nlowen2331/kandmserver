const { _GetPool,_GetWorkerRunning} = require('./writers/DBWorker')
require('dotenv').config()

console.log('Legacy Connection.js is being used by some server component')
console.log('Hooking into DBWorker...')

exports.connection=_GetPool
exports.getIsConnected=_GetWorkerRunning